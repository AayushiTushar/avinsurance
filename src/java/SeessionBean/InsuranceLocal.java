/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SeessionBean;

import entity.BankDetails;
import entity.City;
import entity.Contactus;
import entity.EducationDetails;
import entity.FamilyDetails;
import entity.HealthInfo;
import entity.IdProof;
import entity.IdProofMaster;
import entity.InsuranceType;
import entity.LifeClaim;
import entity.Nominee;
import entity.OccupationMaster;
import entity.PersonalDetails;
import entity.PolicyDetails;
import entity.PremiumDetails;
import entity.RelationDetails;
import entity.State;
import entity.UserPolicyDetails;
import entity.VehicleClaim;
import entity.VehicleDetails;
import entity.VehicleImage;
import entity.Wishlist;
import java.util.Collection;
import java.util.Date;
import javax.ejb.Local;

/**
 *
 * @author Admin
 */
@Local
public interface InsuranceLocal {
    
     // Registration of  User
    
    public void registration(String firstname,String lastname,String username,String email,String password,int roleid);
    
    
     // Insurance 
    
    public void addInsurance(String name,String description);
    public void updateInsurance(int insuranceid,String name,String description);
    public void deleteInsurance(int insuranceid);
    public Collection<InsuranceType> showAllInsurance();
    InsuranceType getInsurance(int insuranceid);
     
    
    // City
    
    public void addCity(String cityname,int stateid);
    public void updateCity(int cityid,String cityname,int stateid);
    public void deleteCity(int cityid);    
    public Collection<City> showAllCity(); 
    City getCity(int cityid);
    
    
    // State
    
    public void addState(String statename);
    public void updateState(int stateid, String statename);
    public void deleteState(int stateid);
    public Collection<State> showAllState();
    State getState(int stateid);  
    
    
    // Policy 
    
    public void addPolicy(String policyname,int primium_year,int max_term,String description,int insuranceid);
    public void updatePolicy(int policyid,String policyname,int primium_year,int max_term,String description,int insuranceid);
    public void deletePolicy(int policyid);   
    public Collection<PolicyDetails> showAllPolicy();
    PolicyDetails getPolicy(int policyid);
    public Collection<PolicyDetails> showAllInsurancePolicy(int insuranceid);
    
    
    //Relation Master    
    
    public void addRelation(String relation);
    public void updateRelation(int relationid,String relation);
    public void deleteRelation(int relationid);
    public Collection<RelationDetails> showAllRelations();
    RelationDetails getRelation(int relationid);
    
    
    //ID Proff Master   
    
    public void addIdProofMaster(String proofname);
    public void updateIdProofMaster(int masterproffid,String proofname);
    public void deleteIdProofMaster(int masterproofid);
    public Collection<IdProofMaster> showAllIdProofTypes();
    IdProofMaster getProofMaster(int masterproofid);
    
    
    // Education
    
    public void addEducation(String lc,String marksheet,String username);
    public Collection<EducationDetails> showEducation();
    
    
    // Personal details
    
    public void updatePersonalDetails(String username,String middlename,String address,String gender,String dob,int age,String phone,int pincode,int cityid ,int stateid,String photo); 
    public Collection<PersonalDetails> showAllPersonalInfo();
     
    
    // Family
    
    public void addFamily(String firstname,String middlename,String lastname,String username,int relationid);
    public Collection<FamilyDetails> showAllFamilyDetails();

    
    // Occupation

    public void addOccupation(String occupation,String officeadd,String officenumber,String username);
    public Collection<OccupationMaster> showAllOccupation();

    
    // IdProof
        
    public void addIdProff(int masterproffid, String number, String image,String username);        
    public Collection<IdProof> showAllIdProffs();    

    
    // Nominee
    
    public void addNominee(String firstname,String middlename,String lastname,String occupation,String officeadd,String officenumber,String lc,String marksheet,String username,int relationid);
    public Collection<Nominee> showAllNominee();
    
    
    // Vehicle details
    
    public void addVehicle(int policyid ,String modelno,String rcbookno,String numberplate,String chesiesno,String bodystyle,String rcbookimg,String username,String boughtyear,String licenceimg);
    public Collection<VehicleDetails> showAllVehicle();
    
    
    // Health details
    
    public void addHealthInfo(String disease,String description,String document,String username);
    public Collection<HealthInfo> showAllHealthInfo();
    
    
    // Contactcus
    
    public void addContactus(String name,String email,String subject,String message);
    public Collection<Contactus> showAllContactDetails();

    
    // Life claim
    
    public void addLifeClaim(int userpolicyid, String deathcertificate,String dateofdeath,String status, String description);
    public Collection<LifeClaim> showAllLifeClaims();
    
    
    // Vehicle cliam
    
    public void addVehicleClaim(int userpolicyid,String description,String status,String vehicalbill);
    public Collection<VehicleClaim> showAllVehicleClaim();
    
    
    // Vehicle Image
    
    public void addVehicleImage(String images,int vclaimid);
    public Collection<VehicleImage> showAllVehicleImage();
        
        
    // Diaplay admin side methods
    
    
    public Collection<UserPolicyDetails> showAllUserPolicy();
    public Collection<BankDetails> showAllBankDetails();   
    public Collection<PremiumDetails> showAllPremiumDetails();    
    public Collection<Wishlist> showAllWishlist();
    
    
}
