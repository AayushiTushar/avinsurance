/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SeessionBean;

import entity.BankDetails;
import entity.City;
import entity.Contactus;
import entity.EducationDetails;
import entity.FamilyDetails;
import entity.HealthInfo;
import entity.IdProof;
import entity.IdProofMaster;
import entity.InsuranceType;
import entity.LifeClaim;
import entity.Nominee;
import entity.OccupationMaster;
import entity.PersonalDetails;
import entity.PolicyDetails;
import entity.PremiumDetails;
import entity.RelationDetails;
import entity.State;
import entity.UserPolicyDetails;
import entity.UserRole;
import entity.VehicleClaim;
import entity.VehicleDetails;
import entity.VehicleImage;
import entity.Wishlist;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Admin
 */
@Stateless
public class Insurance implements InsuranceLocal {

    @PersistenceContext(unitName = "InsurancePU") 
    EntityManager em;

    
    // Registration .... 
    
    @Override
    public void registration(String firstname, String lastname, String username, String email, String password, int roleid) {
      
      UserRole role = (UserRole) em.find(UserRole.class,roleid);
      Collection<PersonalDetails> pds = role.getPersonalDetailsCollection();
      
      PersonalDetails pd = new PersonalDetails();
      pd.setFirstName(firstname);
      pd.setLastName(lastname);
      pd.setUsername(username);
      pd.setEmail(email);
      pd.setPassword(password);
      pd.setRoleId(role);
      pds.add(pd);
      role.setPersonalDetailsCollection(pds);
           
      em.persist(pd);
      em.merge(role);
    }

    
    // Insurance Crud .... 
    
        
    @Override
    public void addInsurance(String name, String description) {
        
        InsuranceType in = new InsuranceType();
        in.setInsuranceName(name);
        in.setDescription(description);             
        em.persist(in);
       
    }

    @Override
    public void updateInsurance(int insuranceid, String name, String description) {
       
        InsuranceType in = (InsuranceType) em.find(InsuranceType.class,insuranceid);
        in.setInsuranceName(name);
        in.setDescription(description);             
        em.merge(in);
       
    }

    @Override
    public void deleteInsurance(int insuranceid) {
       
        InsuranceType in = (InsuranceType) em.find(InsuranceType.class,insuranceid);               
        em.remove(in);
       
    }

    @Override
    public Collection<InsuranceType> showAllInsurance() {
        
        return em.createNamedQuery("InsuranceType.findAll").getResultList();
    }

    @Override
    public InsuranceType getInsurance(int insuranceid) {

        InsuranceType in = em.find(InsuranceType.class, insuranceid);
        return in;
    
    }

    
    // City crud...
    
    
    @Override
    public void addCity(String cityname, int stateid) {
        
        State state=(State) em.find(State.class, stateid);        
        Collection<City> citys=state.getCityCollection();
        City city=new City();
        city.setCityName(cityname);
        city.setStateId(state);
        citys.add(city);
        state.setCityCollection(citys);
        em.persist(city);
        em.merge(state);
    }
    
    @Override
    public void updateCity(int cityid, String cityname, int stateid) {
        
        State state=(State) em.find(State.class, stateid);        
        Collection<City> citys=state.getCityCollection();
        City city= em.find(City.class,cityid);
        city.setCityName(cityname);
        city.setStateId(state);
        citys.add(city);
        state.setCityCollection(citys);
        em.merge(city);
        em.merge(state);
        
    }
    
    @Override
    public void deleteCity(int cityid) {
        
        City city= em.find(City.class,cityid);
        em.remove(city);
    }
    
    @Override
    public Collection<City> showAllCity() {
        
        return em.createNamedQuery("City.findAll").getResultList();
    }

    @Override
    public City getCity(int cityid) {
        
        City city = em.find(City.class, cityid);
        return city;
    }

    
    // State crud ...
    
    
   @Override
    public void addState(String statename) {
        
        State state=new State();
        state.setStateName(statename);
        em.persist(state);
    }
    
    @Override
    public void updateState(int stateid, String statename) {
        
        State state=(State) em.find(State.class,stateid);
        state.setStateName(statename);        
        em.merge(state);
    }
    
    @Override
    public void deleteState(int stateid) {
        
        State state=(State) em.find(State.class,stateid);
        em.remove(state);
    }
    
    @Override
    public Collection<State> showAllState() {
        
        return em.createNamedQuery("State.findAll").getResultList();
    }

    @Override
    public State getState(int stateid) {
    
        State state = em.find(State.class, stateid);
        return state;
    }

    
    // Policy crud ...
    
    
    @Override
    public void addPolicy(String policyname, int primium_year, int max_term, String description, int insuranceid) {
  
        InsuranceType in = (InsuranceType) em.find(InsuranceType.class,insuranceid);
        Collection<PolicyDetails> py =in.getPolicyDetailsCollection();
       
        PolicyDetails p = new PolicyDetails();
        p.setPolicyName(policyname);
        p.setPremiumyearly(primium_year);
        p.setMaxterm(max_term);
        p.setDescription(description);
        p.setInsuranceId(in);
        py.add(p);
        in.setPolicyDetailsCollection(py);
        
        em.persist(p);
        em.merge(in);
         
    }
    
    @Override
    public void updatePolicy(int policyid, String policyname, int primium_year, int max_term, String description, int insuranceid) {
       
        InsuranceType in = (InsuranceType) em.find(InsuranceType.class,insuranceid);
        Collection<PolicyDetails> py =in.getPolicyDetailsCollection();
       
        PolicyDetails p = em.find(PolicyDetails.class,policyid);
        p.setPolicyName(policyname);
        p.setPremiumyearly(primium_year);
        p.setMaxterm(max_term);
        p.setDescription(description);
        p.setInsuranceId(in);
        py.add(p);
        in.setPolicyDetailsCollection(py);
        
        em.merge(p);
        em.merge(in);
    }
    
    @Override
    public void deletePolicy(int policyid) {
        
        PolicyDetails p = (PolicyDetails) em.find(PolicyDetails.class, policyid);
        em.remove(p);
    }
   
    @Override
    public Collection<PolicyDetails> showAllPolicy() {
       
        return em.createNamedQuery("PolicyDetails.findAll").getResultList();
    }
    
    @Override
    public PolicyDetails getPolicy(int policyid) {
        
        PolicyDetails policy = em.find(PolicyDetails.class, policyid);
        return policy;
    }

    @Override
    public Collection<PolicyDetails> showAllInsurancePolicy(int insuranceid) {
         
        InsuranceType in = (InsuranceType) em.find(InsuranceType.class,insuranceid);
        return in.getPolicyDetailsCollection();
       
    }
    
    
    // Relation crud ...
    
    
    @Override
    public void addRelation(String relation) {
        
        RelationDetails relations = new RelationDetails();
        relations.setRelation(relation);
        em.persist(relations);
    }

    @Override
    public void updateRelation(int relationid, String relation) {
        
        RelationDetails relations = (RelationDetails) em.find(RelationDetails.class, relationid);
        relations.setRelation(relation);
        em.merge(relations);
    }

    @Override
    public void deleteRelation(int relationid) {
        
        RelationDetails relations = (RelationDetails) em.find(RelationDetails.class, relationid);        
        em.remove(relations);
        
    }

    @Override
    public Collection<RelationDetails> showAllRelations() {
        
        return em.createNamedQuery("RelationDetails.findAll").getResultList();
    }

    @Override
    public RelationDetails getRelation(int relationid) {
        
        RelationDetails relations = em.find(RelationDetails.class, relationid); 
        return relations;
    }
    
    
    // IdproofMaster crud ...
    
    
    @Override
    public void addIdProofMaster(String proofname) {
        
        IdProofMaster idm = new IdProofMaster();
        idm.setName(proofname);
        em.persist(idm);
        
    }

    @Override
    public void updateIdProofMaster(int masterproofid, String proofname) {
        
        IdProofMaster idm =(IdProofMaster) em.find(IdProofMaster.class,masterproofid);
        idm.setName(proofname);
        em.merge(idm);
        
    }

    @Override
    public void deleteIdProofMaster(int masterproofid) {
        
        IdProofMaster idm =(IdProofMaster) em.find(IdProofMaster.class,masterproofid);
        em.remove(idm);
    }

    @Override
    public Collection<IdProofMaster> showAllIdProofTypes() {
        
        return em.createNamedQuery("IdProofMaster.findAll").getResultList();
    }

    @Override
    public IdProofMaster getProofMaster(int masterproofid) {
        
        IdProofMaster idm = em.find(IdProofMaster.class,masterproofid);
        return idm;
        
    }

    
    // life claim
    
    
    @Override
    public void addLifeClaim(int userpolicyid, String deathcertificate,String dateofdeath,String status, String description) 
    {
       try{
        UserPolicyDetails up = (UserPolicyDetails) em.find(UserPolicyDetails.class, userpolicyid);        
        Collection<LifeClaim> lc= up.getLifeClaimCollection();
        
	LifeClaim l = new LifeClaim();
        
        l.setUserPolicyId(up);
        l.setDeathCertificate(deathcertificate);       
        l.setDateOfDeath(dateofdeath);
        l.setDescription(description);        
        l.setStatus(status);     
        
        lc.add(l);
        up.setLifeClaimCollection(lc);
        em.persist(l);
        em.merge(up);
       }
       catch(Exception e)
       {
           e.printStackTrace();
       }
    }
    
     @Override
    public Collection<LifeClaim> showAllLifeClaims() {
               
        return em.createNamedQuery("LifeClaim.findAll").getResultList();
    }
    
    
    // VEhicle claim ...

    
    @Override
    public void addVehicleClaim(int userpolicyid, String description,String status, String vehicalbill) {
        
        UserPolicyDetails up = (UserPolicyDetails) em.find(UserPolicyDetails.class, userpolicyid);
        
        Collection<VehicleClaim> v= up.getVehicleClaimCollection();
        VehicleClaim vc = new VehicleClaim();
        
        vc.setUserPolicyId(up);
        vc.setVehicleBill(vehicalbill);
        vc.setDescription(description);
        vc.setStatus(status);
        
        v.add(vc);
        up.setVehicleClaimCollection(v);
        em.persist(vc);
        em.merge(up);
    }
        
    @Override
    public Collection<VehicleClaim> showAllVehicleClaim() {

        return em.createNamedQuery("VehicleClaim.findAll").getResultList();
    }  
    
    
    // VehicleImages

    
    @Override
    public void addVehicleImage(String images, int vclaimid) {
        
        VehicleClaim vc = (VehicleClaim) em.find(VehicleClaim.class,vclaimid);
        Collection<VehicleImage> vi = vc.getVehicleImageCollection();
        
        VehicleImage i = new VehicleImage();
        i.setImage(images);
        i.setVehicleClaimId(vc);
        
        vi.add(i);        
        vc.setVehicleImageCollection(vi);
        em.persist(i);
        em.merge(vc); 
        
    }

    @Override
    public Collection<VehicleImage> showAllVehicleImage() {
    
        return em.createNamedQuery("VehicleImage.findAll").getResultList();
    }
    
    
    // Education 

    
    @Override
    public void addEducation(String lc, String marksheet, String username) {

        PersonalDetails pd=(PersonalDetails) em.find(PersonalDetails.class, username);        
        Collection<EducationDetails> eds=pd.getEducationDetailsCollection();
        EducationDetails education=new EducationDetails();
        education.setLCImage(lc);
        education.setMarksheetImage(marksheet);
        education.setUsername(pd);
        eds.add(education);
        pd.setEducationDetailsCollection(eds);
        
        em.persist(education);
        em.merge(pd);
    }
    
    
    @Override
    public Collection<EducationDetails> showEducation() {
       return em.createNamedQuery("EducationDetails.findAll").getResultList();
    }

    
    // Family
    
    
    @Override
    public void addFamily(String firstname, String middlename, String lastname, String username, int relationid) 
    {       

        PersonalDetails pd=(PersonalDetails) em.find(PersonalDetails.class, username); 
        Collection<FamilyDetails> fds = pd.getFamilyDetailsCollection();
        
        RelationDetails rd=(RelationDetails) em.find(RelationDetails.class,relationid);                
        Collection<FamilyDetails> fds1 = rd.getFamilyDetailsCollection();

        FamilyDetails family = new FamilyDetails();
        family.setFirstName(firstname);
        family.setMiddleName(middlename);
        family.setLastName(lastname);
        family.setUsername(pd);
        family.setRelationId(rd);
        
        fds.add(family);
        fds1.add(family);
        
        pd.setFamilyDetailsCollection(fds);
        rd.setFamilyDetailsCollection(fds1);
        
        em.persist(family);
        em.merge(pd);
        em.merge(rd);
           
    }
    
    @Override
    public Collection<FamilyDetails> showAllFamilyDetails() {
        
        return em.createNamedQuery("FamilyDetails.findAll").getResultList();
        
    }

    
    // Occupation
    
    
    @Override
    public void addOccupation(String occupation, String officeadd, String officenumber, String username) {

        PersonalDetails pd=(PersonalDetails) em.find(PersonalDetails.class, username);        
        Collection<OccupationMaster> oms=pd.getOccupationMasterCollection();
        
        OccupationMaster oocupation=new OccupationMaster();
        oocupation.setOccupation(occupation);
        oocupation.setOfficeAddress(officeadd);
        oocupation.setOfficePhoneNumber(officenumber);
        oocupation.setUsername(pd);
                
        oms.add(oocupation);
        pd.setOccupationMasterCollection(oms);
        
        em.persist(oocupation);
        em.merge(pd);
    }
    
    @Override
    public Collection<OccupationMaster> showAllOccupation() {
       
        return em.createNamedQuery("OccupationMaster.findAll").getResultList();
        
    }
    

    // Personal details
    
    
     @Override
    public void updatePersonalDetails(String username,String middlename, String address, String gender, String dob, int age, String phone, int pincode, int cityid, int stateid, String photo) {
        
        try{
        State state=(State) em.find(State.class,stateid);
        Collection<PersonalDetails> pds = state.getPersonalDetailsCollection();
        City city = (City) em.find(City.class,cityid);
        Collection<PersonalDetails> pds1 = city.getPersonalDetailsCollection();
        
        PersonalDetails pd = (PersonalDetails) em.find(PersonalDetails.class,username);
        pd.setMiddleName(middlename);
        pd.setGender(gender);
        pd.setAge(age);      
        System.out.println("new Date:"+ dob);       
        pd.setDob(dob);
        pd.setPhoneNumber(phone);        
        pd.setAddress(address);
        pd.setPinCode(pincode);
        pd.setCityId(city);
        pd.setStateId(state);
        pd.setPhotoImage(photo);
        
        pds.add(pd);
        pds1.add(pd);
        
        state.setPersonalDetailsCollection(pds);
        city.setPersonalDetailsCollection(pds1);
        
        em.merge(pd);
        em.merge(city);
        em.merge(state);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public Collection<PersonalDetails> showAllPersonalInfo() {
        
        return em.createNamedQuery("PersonalDetails.findAll").getResultList();
    }
    
    
    // Id Proof
    
    
    @Override
    public void addIdProff(int masterproffid, String number, String image, String username) {

        IdProofMaster idm = (IdProofMaster) em.find(IdProofMaster.class,masterproffid);
        Collection<IdProof> idps = idm.getIdProofCollection();
        PersonalDetails pd = (PersonalDetails) em.find(PersonalDetails.class,username);
        Collection<IdProof> idps1 = pd.getIdProofCollection();
        
        IdProof idp = new IdProof();
        idp.setMasterProofId(idm);
        idp.setNumber(number);
        idp.setImage(image);
        
        idps.add(idp);
        idps1.add(idp);
        
        idm.setIdProofCollection(idps);
        pd.setIdProofCollection(idps1);
        
        em.persist(idp);
        em.merge(idm);
        em.merge(pd);
        
    }
    
    @Override
    public Collection<IdProof> showAllIdProffs() {

        return em.createNamedQuery("IdProofMaster.findAll").getResultList();
    }

    
    // Nominee
    
    
    @Override
    public void addNominee(String firstname, String middlename, String lastname, String occupation, String officeadd, String officenumber, String lc, String marksheet, String username, int relationid) {
        
        RelationDetails rd = (RelationDetails) em.find(RelationDetails.class,relationid);
        Collection<Nominee> noms = rd.getNomineeCollection();
        PersonalDetails pd = (PersonalDetails) em.find(PersonalDetails.class,username);
        Collection<Nominee> noms1 = pd.getNomineeCollection();
        
        Nominee nominee = new Nominee();
        nominee.setFirstName(firstname);
        nominee.setMiddleName(middlename);
        nominee.setLastName(lastname);
        nominee.setOccupation(occupation);
        nominee.setOfficeAddress(officeadd);
        nominee.setOfficeNumber(officenumber);
        nominee.setLCImage(lc);
        nominee.setMarksheetImage(marksheet);
        nominee.setRelationId(rd);
        nominee.setUsername(pd);
        
        noms.add(nominee);
        noms1.add(nominee);
        
        rd.setNomineeCollection(noms);
        pd.setNomineeCollection(noms1);
        
        em.persist(nominee);
        em.merge(rd);
        em.merge(pd);
        
    }
        
    @Override
    public Collection<Nominee> showAllNominee() {
       
        return em.createNamedQuery("Nominee.findAll").getResultList();
        
    }

    
   // Vehicle details
    
    
    @Override
    public void addVehicle(int policyid, String modelno, String rcbookno, String numberplate, String chesiesno, String bodystyle, String rcbookimg,String username, String boughtyear, String licenceimg) 
    {
    
        try
        {
        PersonalDetails pd = (PersonalDetails) em.find(PersonalDetails.class,username);
        Collection<VehicleDetails> vd = pd.getVehicleDetailsCollection();
        PolicyDetails pod = (PolicyDetails) em.find(PolicyDetails.class,policyid);
        Collection<VehicleDetails> vd1 = pd.getVehicleDetailsCollection();
        
        VehicleDetails vehicle = new VehicleDetails();
        vehicle.setPolicyId(pod);
        vehicle.setModelNumber(modelno);
        vehicle.setRCBookNumber(rcbookno);
        vehicle.setNumberPlate(numberplate);
        vehicle.setChesiesNumber(chesiesno);
        vehicle.setBodyStyle(bodystyle);
        vehicle.setRCBookImage(rcbookimg);
        vehicle.setUsername(pd);       
        vehicle.setBoughtYear(boughtyear);
        vehicle.setLicenceImage(licenceimg);
        
        vd.add(vehicle);
        vd1.add(vehicle);
        
        pd.setVehicleDetailsCollection(vd);
        pod.setVehicleDetailsCollection(vd1);
        
        em.persist(vehicle);
        em.merge(pd);
        em.merge(pod);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
    }
    
    @Override
    public Collection<VehicleDetails> showAllVehicle() {
        
        return em.createNamedQuery("VehicleDetails.findAll").getResultList();
        
    }
    
    
    // Healthinfo

    
    @Override
    public void addHealthInfo(String disease, String description, String document, String username) {
        
        PersonalDetails pd = (PersonalDetails) em.find(PersonalDetails.class,username);
        Collection<HealthInfo> hinfos = pd.getHealthInfoCollection();
        
        HealthInfo health = new HealthInfo();
        health.setDisease(disease);
        health.setDescription(description);
        health.setDocument(document);
        health.setUsername(pd);
        
        hinfos.add(health);
        pd.setHealthInfoCollection(hinfos);
        em.persist(health);
        em.merge(pd);
        
        
    }
    
     @Override
    public Collection<HealthInfo> showAllHealthInfo() {
        
        return em.createNamedQuery("HealthInfo.findAll").getResultList();
    }

    
    // Contactus

    
    @Override
    public void addContactus(String name, String email, String subject, String message) {
        
        Contactus contact = new Contactus();
        contact.setName(name);
        contact.setEmail(email);
        contact.setSubject(subject);
        contact.setMessage(message);
        em.persist(contact);
    }

    @Override
    public Collection<Contactus> showAllContactDetails() {
        
        return em.createNamedQuery("Contactus.findAll").getResultList();
    }
    
    
    

    
    // Display all admin side methods .... 

   
    @Override
    public Collection<UserPolicyDetails> showAllUserPolicy() {
        
        return em.createNamedQuery("UserPolicyDetails.findAll").getResultList();
    }

    @Override
    public Collection<BankDetails> showAllBankDetails() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

    @Override
    public Collection<PremiumDetails> showAllPremiumDetails() {
        
        return em.createNamedQuery("PremiumDetails.findAll").getResultList();
        
    }
    
    @Override
    public Collection<Wishlist> showAllWishlist() {
        
        return em.createNamedQuery("Wishlist.findAll").getResultList();
        
    }

   

    
    

}
