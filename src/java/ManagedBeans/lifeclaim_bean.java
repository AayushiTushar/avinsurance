/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import entity.LifeClaim;
import entity.UserPolicyDetails;
import helper.session;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Random;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Named(value = "lifeclaim_bean")
@RequestScoped
public class lifeclaim_bean {

    Response res;   
    Insurance_Client client;
    Collection<LifeClaim> lifeclaims;
    GenericType<Collection<LifeClaim>> glifeclaims;
    
    Collection<UserPolicyDetails> upolicy;
    GenericType<Collection<UserPolicyDetails>> gupolicy;
    
    private static final String FolderPath = "F:\\Practical_SEM-8\\Insurance_Project\\AVInsurance_Project\\web\\Images\\Claim\\";
    
    private String certynum,dodeath,desc;
    private String status = "Pending";
    private Part deathimg;
    private int userpid;
    private String username = session.getUsername();

    
    public String getCertynum() {
        return certynum;
    }

    public void setCertynum(String certynum) {
        this.certynum = certynum;
    }

    public String getDodeath() {
        return dodeath;
    }

    public void setDodeath(String dodeath) {
        this.dodeath = dodeath;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Part getDeathimg() {
        return deathimg;
    }

    public void setDeathimg(Part deathimg) {
        this.deathimg = deathimg;
    }

    public int getUserpid() {
        return userpid;
    }

    public void setUserpid(int userpid) {
        this.userpid = userpid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    
    
    public lifeclaim_bean() 
    {
        System.out.println("Hello lifeclaim Bean ");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	String token="";

        token = request.getSession().getAttribute("token").toString();
       System.out.println("Token="+token);
       
        client = new Insurance_Client(token);
                
        lifeclaims = new ArrayList<LifeClaim>();
        glifeclaims = new GenericType<Collection<LifeClaim>>(){};
        
       upolicy = new ArrayList<UserPolicyDetails>();
       gupolicy = new GenericType<Collection<UserPolicyDetails>>(){};
    }
    
    public void insertClaimofLife()
    {
        for(UserPolicyDetails upd : showUsersPolicy() )
        {
            if(getCertynum().equals(upd.getPolicyCertificateNumber()))
            {
                this.userpid = upd.getUserPolicyId();
                System.err.println(userpid);
            }
        }
        
        System.err.println("Called lifeclaim insert");
                     
        String DeathImage = "";
        try{
            InputStream input = deathimg.getInputStream();            
            Random mRandom = new Random();
            StringBuilder sb = new StringBuilder();
            sb.append(mRandom.nextInt(9) + 1);
            for(int i=0; i < 11; i++){
                sb.append(mRandom.nextInt(10));
            }
            String tempStr = sb.toString();

            DeathImage = tempStr + deathimg.getSubmittedFileName();
            System.err.println("DeathImage name = "+ DeathImage);             
            Files.copy(input, new File(FolderPath+ "" + DeathImage).toPath());
            
        }
        catch(Exception ex)
        {
            System.err.println("Exception while file upload " + ex.toString());
        }
            
        //client.addLifeClaim(upid, deathimg, dodeath, status, desc);
        
    }
    
    public Collection<LifeClaim> showLifeClaim()
    {
       res = client.showAllLifeClaims(Response.class);
       lifeclaims = res.readEntity(glifeclaims);
        return lifeclaims;
    }
    
    public Collection<UserPolicyDetails> showUsersPolicy()
    {
       res = client.showAllUserPolicy(Response.class);
       upolicy = res.readEntity(gupolicy);
        return upolicy;
    }
    
}
