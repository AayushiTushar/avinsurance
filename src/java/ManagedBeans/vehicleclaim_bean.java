/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import entity.VehicleClaim;
import helper.session;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Named(value = "vehicleclaim_bean")
@RequestScoped
public class vehicleclaim_bean {

    Response res;   
    Insurance_Client client;
    Collection<VehicleClaim> vehicleclaims;
    GenericType<Collection<VehicleClaim>> gvehicleclaims;
    
    private static final String FolderPath = "F:\\Practical_SEM-8\\Insurance_Project\\AVInsurance_Project\\web\\Images\\Claim\\";
    
    private String desc,status,certynum;
    private Part bill;
    private int userpid;
    private String username = session.getUsername();

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCertynum() {
        return certynum;
    }

    public void setCertynum(String certynum) {
        this.certynum = certynum;
    }

    public Part getBill() {
        return bill;
    }

    public void setBill(Part bill) {
        this.bill = bill;
    }

    public int getUserpid() {
        return userpid;
    }

    public void setUserpid(int userpid) {
        this.userpid = userpid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
 
    
    
    public vehicleclaim_bean() 
    {
        System.out.println("Hello vehicleclaim Bean ");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	String token="";

        token = request.getSession().getAttribute("token").toString();
       System.out.println("Token="+token);
       
        client = new Insurance_Client(token);
                
        vehicleclaims = new ArrayList<VehicleClaim>();
        gvehicleclaims = new GenericType<Collection<VehicleClaim>>(){};
    }
    
    
    public void insertClaimofVehicle()
    {
        System.err.println("Called vehicleclaim insert");
                     
        String BillImg = "";
        try{
            InputStream input = bill.getInputStream();            
            Random mRandom = new Random();
            StringBuilder sb = new StringBuilder();
            sb.append(mRandom.nextInt(9) + 1);
            for(int i=0; i < 11; i++){
                sb.append(mRandom.nextInt(10));
            }
            String tempStr = sb.toString();

            BillImg = tempStr + bill.getSubmittedFileName();
            System.err.println("Bill = "+ BillImg);             
            Files.copy(input, new File(FolderPath+ "" + BillImg).toPath());
            
        }
        catch(Exception ex)
        {
            System.err.println("Exception while file upload " + ex.toString());
        }
                
        
        //client.addLifeClaim(upid, deathimg, dodeath, status, desc);
        
    }
    
    public Collection<VehicleClaim> showVehicleClaim()
    {
       res = client.showAllVehicleClaim(Response.class);
       vehicleclaims = res.readEntity(gvehicleclaims);
        return vehicleclaims;
    }
}
