/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.InsuranceClient;
import helper.passwordhelper;
import javax.inject.Named;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;


/**
 *
 * @author Admin
 */
@Named(value = "registerbean")
@RequestScoped
public class registerbean implements Serializable {
  
   private String firstname,lastname,email,password,username,pass1;
   private int roleid = 2;
    
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass1() {
        return pass1;
    }

    public void setPass1(String pass1) {
        this.pass1 = pass1;
    }

    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    
    
    public void register()
    {

        InsuranceClient client;
        
        client = new InsuranceClient();
        
        if(pass1.equals(password))
        {
            client.registration(this.firstname, this.lastname, this.username, this.email,passwordhelper.encodpassword(this.password),String.valueOf(this.roleid));
            System.out.println("Record is Inserted.....");
            System.out.println(getFirstname() +" "+ getLastname() +" "+ getEmail() +" "+ getUsername() +" "+ getPassword() +" "+ getRoleid());
 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully!", "Your account was registed... "));
     

        }
        else
        {
//             System.out.println("Plz check yr password.....");
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Check your Password "));
        }
    }
    
   
    
}
