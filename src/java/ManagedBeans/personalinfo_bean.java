/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import entity.IdProof;
import entity.PersonalDetails;
import helper.session;
import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Random;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Named(value = "personalinfo_bean")
@RequestScoped
public class personalinfo_bean {

  
    public personalinfo_bean() 
    {
        
        System.out.println("Hello PersonalInfo Bean .............................");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        String token="";

        token = request.getSession().getAttribute("token").toString();
        System.out.println("Token="+token);

        client = new Insurance_Client(token);
           
        users = new ArrayList<PersonalDetails>();
        gusers = new GenericType<Collection<PersonalDetails>>(){}; 
        
        ids = new ArrayList<IdProof>();
        gids = new GenericType<Collection<IdProof>>(){};
        
    }
    
    Response res;   
    Insurance_Client client;
    Collection<PersonalDetails> users;
    GenericType<Collection<PersonalDetails>> gusers;
    
    Collection<IdProof> ids;
    GenericType<Collection<IdProof>> gids;
    
    private static final String FolderPath = "F:\\Practical_SEM-8\\Insurance_Project\\AVInsurance_Project\\web\\Images\\Users\\";
    // personal
    
    private String username = session.getUsername();
    private String mname,add,gender,phone,dob;
    private int age,pincode,cityid,stateid;    
    private Part photoimg;    
    
    // IdProof
    
    private String number;
    private int masterproofid;
    private Part proofimg;

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }  

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPincode() {
        return pincode;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public int getCityid() {
        return cityid;
    }

    public void setCityid(int cityid) {
        this.cityid = cityid;
    }

    public int getStateid() {
        return stateid;
    }

    public void setStateid(int stateid) {
        this.stateid = stateid;
    }
   
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }    

    public int getMasterproofid() {
        return masterproofid;
    }

    public void setMasterproofid(int masterproofid) {
        this.masterproofid = masterproofid;
    }

    public Part getPhotoimg() {
        return photoimg;
    }

    public void setPhotoimg(Part photoimg) {
        this.photoimg = photoimg;
    }

    public Part getProofimg() {
        return proofimg;
    }

    public void setProofimg(Part proofimg) {
        this.proofimg = proofimg;
    }
        
    
    public void updateDetail()
    {
        System.err.println("Heloo....in Personal....");
        String PhotoImage ="";
        try{
            InputStream input = photoimg.getInputStream();             
            Random mRandom = new Random();
            StringBuilder sb = new StringBuilder();
            sb.append(mRandom.nextInt(9) + 1);
            for(int i=0; i < 11; i++){
                sb.append(mRandom.nextInt(10));
            }
            String tempStr = sb.toString();

            PhotoImage = tempStr + photoimg.getSubmittedFileName();                      
            Files.copy(input, new File(FolderPath+ "" + PhotoImage).toPath());
        }
        catch(Exception ex)
        {
            System.err.println("Exception while file upload " + ex.toString());
        }
        
      
        System.err.println("Before Update .............................");  
        System.err.println(username);
        System.err.println(mname);
        System.err.println(add);
        System.err.println(gender);
        System.err.println(dob);
        System.err.println(age);
        System.err.println(phone);
        System.err.println(pincode);
        System.err.println(cityid);
        System.err.println(stateid);
        System.err.println(PhotoImage);
        client.updateInfo(username, mname, add, gender,dob,String.valueOf(age), phone, String.valueOf(pincode),String.valueOf(cityid) ,String.valueOf(stateid), PhotoImage);
        System.err.println("After Update...........................");    
         System.err.println(username);
        System.err.println(mname);
        System.err.println(add);
        System.err.println(gender);
        System.err.println(dob);
        System.err.println(age);
        System.err.println(phone);
        System.err.println(pincode);
        System.err.println(cityid);
        System.err.println(stateid);
        System.err.println(PhotoImage);
        
//        client.addIdProff(String.valueOf(masterproofid), number,ProofImage, username);
//        System.err.println("Insert.......");
//        
    }
    
    public void updateInfo()
    {
        System.err.println("Update Information. ............");
//          String PhotoImage = "";
//        String ProofImage = "";
//        try{
//            InputStream input = photoimg.getInputStream(); 
//            InputStream input1 = proofimg.getInputStream(); 
//            Random mRandom = new Random();
//            StringBuilder sb = new StringBuilder();
//            sb.append(mRandom.nextInt(9) + 1);
//            for(int i=0; i < 11; i++){
//                sb.append(mRandom.nextInt(10));
//            }
//            String tempStr = sb.toString();
//
//            PhotoImage = tempStr + photoimg.getSubmittedFileName();
//            ProofImage = tempStr + proofimg.getSubmittedFileName();
//            System.err.println("PhotoImg name = "+ PhotoImage); 
//            System.err.println("ProofImg name = "+ ProofImage); 
//
//            Files.copy(input, new File(FolderPath+ "" + PhotoImage).toPath());
//            Files.copy(input1, new File(FolderPath+ "" + ProofImage).toPath());
//        }      
    }
    
    public Collection<PersonalDetails> showPersonal()
    {
       res = client.showAllPersonalInfo(Response.class);
        users = res.readEntity(gusers);
        return users;
    }
    
    public Collection<IdProof> showIdProofs()
    {
       res = client.showAllIdProffs(Response.class);
        ids = res.readEntity(gids);
        return ids;
    }
 
   
}
