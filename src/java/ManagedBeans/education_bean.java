/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import entity.EducationDetails;
import helper.session;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Named(value = "education_bean")
@RequestScoped
public class education_bean {

    Response res;   
    Insurance_Client client;
    Collection<EducationDetails> eds;
    GenericType<Collection<EducationDetails>> geds;
    
    private static final String FolderPath = "F:\\Practical_SEM-8\\Insurance_Project\\AVInsurance_Project\\web\\Images\\Users\\";
    
    private Part lcimg,marksheetimg;
    private String Usernme = session.getUsername();
    
    public Part getLcimg() {
        return lcimg;
    }

    public void setLcimg(Part lcimg) {
        this.lcimg = lcimg;
    }

    public Part getMarksheetimg() {
        return marksheetimg;
    }

    public void setMarksheetimg(Part marksheetimg) {
        this.marksheetimg = marksheetimg;
    }

    public String getUsernme() {
        return Usernme;
    }

    public void setUsernme(String Usernme) {
        this.Usernme = Usernme;
    }
    
    

    public education_bean() {
        
        System.out.println("Hello Education Bean ");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        String token="";

        token = request.getSession().getAttribute("token").toString();
       System.out.println("Token="+token);
        
        //String token1 = request.getHeader("Authorization").substring("Bearer ".length());
      // System.out.println("Token="+token1);
        client = new Insurance_Client(token);
           
      // brc = new BookRestClient(username, password);
        eds = new ArrayList<EducationDetails>();
        geds = new GenericType<Collection<EducationDetails>>(){};
        
    }
    
    public void insertEducation()
    {
        String LcImage = "";
        String MarksheetImage = "";
        try{
                InputStream input = lcimg.getInputStream(); 
                InputStream input1 = marksheetimg.getInputStream(); 
                Random mRandom = new Random();
                StringBuilder sb = new StringBuilder();
                sb.append(mRandom.nextInt(9) + 1);
                for(int i=0; i < 11; i++){
                    sb.append(mRandom.nextInt(10));
                }
                String tempStr = sb.toString();
                
                LcImage = tempStr + lcimg.getSubmittedFileName();
                MarksheetImage = tempStr + marksheetimg.getSubmittedFileName();
                System.err.println("Lc name = "+ LcImage); 
                System.err.println("Marksheet name = "+ MarksheetImage); 
                
                Files.copy(input, new File(FolderPath+ "" + LcImage).toPath());
                Files.copy(input1, new File(FolderPath+ "" + MarksheetImage).toPath());
        }
        catch(Exception ex)
        {
            System.err.println("Exception while file upload " + ex.toString());
        }
        
        client.addEducation(LcImage, MarksheetImage,Usernme);
        //System.err.println("LC ="+ LcImage +"MS ="+ MarksheetImage);
    }
    
    
    public Collection<EducationDetails> showDetails()
    {
        res = client.showEducation(Response.class);
        eds = res.readEntity(geds);
        return eds;
    }
    
}
