/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import entity.RelationDetails;
import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Named(value = "relation_bean")
@RequestScoped
public class relation_bean {

    Response res;   
    Insurance_Client client;
    Collection<RelationDetails> relations;
    GenericType<Collection<RelationDetails>> grelations;
    
    RelationDetails currentrelations;
    GenericType<RelationDetails> gCurrrelations;
    
    private String name;
    private int relationid;

    
    public int getRelationid() {
        return relationid;
    }

    public void setRelationid(int relationid) {
        this.relationid = relationid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RelationDetails getCurrentrelations() {
        return currentrelations;
    }

    public void setCurrentrelations(RelationDetails currentrelations) {
        this.currentrelations = currentrelations;
    }

    public GenericType<RelationDetails> getgCurrrelations() {
        return gCurrrelations;
    }

    public void setgCurrrelations(GenericType<RelationDetails> gCurrrelations) {
        this.gCurrrelations = gCurrrelations;
    }
    

    public relation_bean() 
    {
        System.out.println("Hello Relation Bean ");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		String token="";

        token = request.getSession().getAttribute("token").toString();
       System.out.println("Token="+token);
        
        //String token1 = request.getHeader("Authorization").substring("Bearer ".length());
      // System.out.println("Token="+token1);
        client = new Insurance_Client(token);
           
      // brc = new BookRestClient(username, password);
        relations = new ArrayList<RelationDetails>();
        grelations = new GenericType<Collection<RelationDetails>>(){};
        
        currentrelations = new RelationDetails();
        gCurrrelations = new GenericType<RelationDetails>(){};
    }
    
    
     public Collection<RelationDetails> getRelation()
    {        
        res = client.showAllRelations(Response.class);
        relations = res.readEntity(grelations);
        return relations;
    }
    
    
     public String insertRelation(){
        
         System.out.println("Relation id =" + getRelationid());
        if(getRelationid()>0)
        {
            System.err.println("Called relation update");
            
            System.err.println("" + this.relationid);
            client.updateRelation(String.valueOf(relationid), name);
            return "RelationDetail.jsf?faces-redirect=true";
            
        }
        else
        {
            
            System.err.println("Called relation insert");
            client.addRelation(name);
            return "RelationDetail.jsf?faces-redirect=true";
            
        }
    }
     
     public String removeRelation(){
       
        System.err.println("delete Relation");
        client.deleteRelation(String.valueOf(relationid));
        return "RelationDetail.jsf?faces-redirect=true";
        
    }
     
    public String getDetails(int id)
    {
        res = client.getRelation(Response.class,id+"");
        currentrelations = res.readEntity(gCurrrelations);
        this.name =  currentrelations.getRelation();
        this.relationid = currentrelations.getRelationId();

        return "RelationForm.jsf";
        
    }
    
}




