/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import entity.EducationDetails;
import entity.FamilyDetails;
import helper.session;
import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Named(value = "family_bean")
@RequestScoped
public class family_bean {

    private String fname,lname,mname;
    private String username = session.getUsername();
    private int relationid;
    
    Response res;   
    Insurance_Client client;
    Collection<FamilyDetails> fds;
    GenericType<Collection<FamilyDetails>> gfds;

    
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getRelationid() {
        return relationid;
    }

    public void setRelationid(int relationid) {
        this.relationid = relationid;
    }
    

    public family_bean() 
    {
        System.out.println("Hello Family Bean ");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        String token="";

        token = request.getSession().getAttribute("token").toString();
       System.out.println("Token="+token);
        
      
        client = new Insurance_Client(token);
      
        fds = new ArrayList<FamilyDetails>();
        gfds = new GenericType<Collection<FamilyDetails>>(){};
    }
    
    
    public Collection<FamilyDetails> DisplayFamily()
    {
        res = client.showAllFamilyDetails(Response.class);
        fds = res.readEntity(gfds);
        return fds;
    }
    
    public void insertFamilyDetails()
    {        
        client.addFamily(fname, mname, lname, username,String.valueOf(relationid));
        System.err.println("Insert Family...");
    }
    
}
