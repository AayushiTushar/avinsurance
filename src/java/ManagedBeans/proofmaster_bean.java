/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import entity.IdProofMaster;
import entity.RelationDetails;
import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Named(value = "proofmaster_bean")
@RequestScoped
public class proofmaster_bean {

    Response res;   
    Insurance_Client client;
    Collection<IdProofMaster> mids;
    GenericType<Collection<IdProofMaster>> gmids;
    
    IdProofMaster currentmids;
    GenericType<IdProofMaster> gCurrmids;
    
    private String name;
    private int masterid;

    public IdProofMaster getCurrentmids() {
        return currentmids;
    }

    public void setCurrentmids(IdProofMaster currentmids) {
        this.currentmids = currentmids;
    }

    public GenericType<IdProofMaster> getgCurrmids() {
        return gCurrmids;
    }

    public void setgCurrmids(GenericType<IdProofMaster> gCurrmids) {
        this.gCurrmids = gCurrmids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMasterid() {
        return masterid;
    }

    public void setMasterid(int masterid) {
        this.masterid = masterid;
    }
        

    public proofmaster_bean() 
    {
        System.out.println("Hello proofmaster Bean ");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		String token="";

        token = request.getSession().getAttribute("token").toString();
       System.out.println("Token="+token);
        
        //String token1 = request.getHeader("Authorization").substring("Bearer ".length());
      // System.out.println("Token="+token1);
        client = new Insurance_Client(token);
           
      // brc = new BookRestClient(username, password);
        mids = new ArrayList<IdProofMaster>();
        gmids = new GenericType<Collection<IdProofMaster>>(){};
        
        currentmids = new IdProofMaster();
        gCurrmids = new GenericType<IdProofMaster>(){};
    }
    
    
    public Collection<IdProofMaster> getMasterProof()
    {        
        res = client.showAllIdProofTypes(Response.class);
        mids = res.readEntity(gmids);
        return mids;
    }
    
    
     public String insertMasterProof(){
        
         System.out.println("Proof id =" + getMasterid());
        if(getMasterid()>0)
        {
            System.err.println("Called MasterProof update");
            
            System.err.println("" + this.masterid);
            client.updateIdProofMaster(String.valueOf(masterid), name);
            return "Id_proff_master.jsf?faces-redirect=true";
            
        }
        else
        {
            
            System.err.println("Called MasterProof insert");
            client.addRelation(name);
            return "Id_proff_master.jsf?faces-redirect=true";
            
        }
    }
     
     public String removeMasterProof(){
       
        System.err.println("delete MasterProof");
        client.deleteIdProofMaster(String.valueOf(masterid));
        return "Id_proff_master.jsf?faces-redirect=true";
        
    }
     
    public String dataOfProof(int pid)
    {
    
        res = client.getProofMaster(Response.class,pid+"");
        currentmids = res.readEntity(gCurrmids);
        this.name =  currentmids.getName();
        this.masterid = currentmids.getMasterProofId();

        return "Id_Proof_Master_Form.jsf";
              
    }
       
    
}
