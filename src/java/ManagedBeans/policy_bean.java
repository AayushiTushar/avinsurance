/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import entity.IdProofMaster;
import entity.PolicyDetails;
import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Named(value = "policy_bean")
@RequestScoped
public class policy_bean {

    Response res;   
    Insurance_Client client;
    Collection<PolicyDetails> ps;
    GenericType<Collection<PolicyDetails>> gps;
    
    PolicyDetails currentps;
    GenericType<PolicyDetails> gCurrps;
    
    private String name,desc;
    private int insuranceid,policyid,year,term;

    
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public int getPolicyid() {
        return policyid;
    }

    public void setPolicyid(int policyid) {
        this.policyid = policyid;
    }    
    
    public PolicyDetails getCurrentps() {
        return currentps;
    }

    public void setCurrentps(PolicyDetails currentps) {
        this.currentps = currentps;
    }

    public GenericType<PolicyDetails> getgCurrps() {
        return gCurrps;
    }

    public void setgCurrps(GenericType<PolicyDetails> gCurrps) {
        this.gCurrps = gCurrps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
   
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }   

    public int getInsuranceid() {
        return insuranceid;
    }

    public void setInsuranceid(int insuranceid) {
        this.insuranceid = insuranceid;
    }
     
    
    public policy_bean() 
    {
         System.out.println("Hello policy Bean ");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        String token="";

        token = request.getSession().getAttribute("token").toString();
       System.out.println("Token="+token);
        
        //String token1 = request.getHeader("Authorization").substring("Bearer ".length());
      // System.out.println("Token="+token1);
        client = new Insurance_Client(token);
           
      // brc = new BookRestClient(username, password);
        ps = new ArrayList<PolicyDetails>();
        gps = new GenericType<Collection<PolicyDetails>>(){};
        
        currentps = new PolicyDetails();
        gCurrps = new GenericType<PolicyDetails>(){};
    }
    
    public Collection<PolicyDetails> getPolicies()
    {        
        res = client.showAllPolicy(Response.class);
        ps = res.readEntity(gps);
        return ps;
    }
    
    
     public String insertPolicies(){
        
        System.out.println("policy id =" + getPolicyid());
        if(getPolicyid()>0)
        {
            System.err.println("Called Policy update");
            
            System.err.println("" + this.policyid);
            client.updatePolicy(String.valueOf(policyid), name,String.valueOf(year),String.valueOf(term),desc,String.valueOf(insuranceid));
            return "PolicyDetails.jsf?faces-redirect=true";
            
        }
        else
        {
            
            System.err.println("Called policy insert");
            client.addPolicy(name,String.valueOf(year),String.valueOf(term),desc,String.valueOf(insuranceid));
            return "PolicyDetails.jsf?faces-redirect=true";
            
        }
    }
     
     public String removePolicy(){
       
        System.err.println("delete policy");
        client.deleteIdProofMaster(String.valueOf(policyid));
        return "PolicyDetails.jsf?faces-redirect=true";
        
    }
     
    public String getPolicy(int pid)
    {
    
        res = client.getPolicy(Response.class,pid+"");
        currentps = res.readEntity(gCurrps);
        this.name = currentps.getPolicyName();
        this.year = currentps.getPremiumyearly();
        this.desc = currentps.getDescription();
        this.term = currentps.getMaxterm();
        this.insuranceid = currentps.getInsuranceId().getInsuranceId();
        this.policyid = currentps.getPolicyId();
                    
        return "PolicyForm.jsf";
        
        
    }
    
}
