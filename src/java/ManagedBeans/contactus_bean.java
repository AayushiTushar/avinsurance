/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.InsuranceClient;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;


/**
 *
 * @author Admin
 */
@Named(value = "contactus_bean")
@RequestScoped
public class contactus_bean {

    private String name,email,sub,desc;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
   
    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
   
//    Collection<Contactus> cus;
//    GenericType<Collection<Contactus>> gcus;
    
    public contactus_bean() 
    {
        System.out.println("Hello Contactus Bean ");
//        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//        String token="";
//
//        token = request.getSession().getAttribute("token").toString();
//       System.out.println("Token="+token);
//        
//      
//        client = new Insurance_Client(token);
//      
//        cus = new ArrayList<Contactus>();
//        gcus = new GenericType<Collection<Contactus>>(){};
    }
    
//     public Collection<Contactus> DisplayContactus()
//    {
//        res = client.showAllFamilyDetails(Response.class);
//        cus = res.readEntity(gcus);
//        return cus;
//    }
    
    public void insertContactus()
    {    
        InsuranceClient client1;        
        client1 = new InsuranceClient();
        
        client1.addContactus(name, email, sub, desc);
        System.err.println("Insert Contactus...");
        
    }
    
}
