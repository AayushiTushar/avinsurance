/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import java.util.Set;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationStatus;
import static javax.security.enterprise.AuthenticationStatus.SEND_CONTINUE;
import javax.security.enterprise.SecurityContext;
import static javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters.withParams;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.credential.Password;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
@Named(value = "loginbean")
@RequestScoped
public class loginbean {

    @Inject private SecurityContext securityContext;
    
    private String username;
    private String password;    
    private AuthenticationStatus status;
    private Set<String> roles;
 
    public loginbean() {
    }

    public SecurityContext getSecurityContext() {
        return securityContext;
    }

    public void setSecurityContext(SecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AuthenticationStatus getStatus() {
        return status;
    }

    public void setStatus(AuthenticationStatus status) {
        this.status = status;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
    
    public String dologin(){
        
         FacesContext context = FacesContext.getCurrentInstance();
        try
        {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

            request.getSession().setAttribute("logged-group", ""); 

            Credential credential = new UsernamePasswordCredential(username, new Password(password));
            AuthenticationStatus status= securityContext.authenticate(request, response, withParams().credential(credential));
                                           
     
            if (status.equals(SEND_CONTINUE)) {
                 // Authentication mechanism has send a redirect, should not
                 // send anything to response from JSF now. The control will now go into HttpAuthenticationMechanism
                 context.responseComplete();
            } 
       
 
            System.out.println("In bean");
            request.getSession().setAttribute("username", this.username);
            
            if(roles.contains("Admin"))
            {
                System.out.println("In admin");
                request.getSession().setAttribute("logged-group", "Admin");                
                return "/admin/AdminPage.jsf?faces-redirect=true";
            }
            else if(roles.contains("User"))
            {
                System.out.println("In user");
                request.getSession().setAttribute("logged-group", "User");                
                return "Home.jsf?faces-redirect=true";
            }
        }
        catch (Exception e)
        {
            // message = "Out- Either user or login is wrong !!!";
             e.printStackTrace();
        }
      return "/Login.jsf?faces-redirect=true";
        
    }
    
    
    public String logout() throws ServletException
    {
        System.out.println("In Log out");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        request.getSession().setAttribute("logged-group", "");
        request.logout();
        request.getSession().invalidate();
        return "/Login.jsf?faces-redirect=true";
    }
    
    
    public String Adminlogout() throws ServletException
    {
        System.out.println("In Log out");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        request.getSession().setAttribute("logged-group", "");
        request.logout();
        request.getSession().invalidate();
        return "/Login.jsf?faces-redirect=true";
    }
    
    
}
