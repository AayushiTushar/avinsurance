/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import helper.session;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Admin
 */
@Named(value = "homebean")
@RequestScoped
public class homebean {

    private String username;
    /**
     * Creates a new instance of homebean
     */
    public homebean() {
    }
    
     public String getUsername() {
       return session.getUsername();
    }
}
