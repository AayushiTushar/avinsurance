/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import entity.FamilyDetails;
import entity.OccupationMaster;
import helper.session;
import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Named(value = "occupation_bean")
@RequestScoped
public class occupation_bean {

    private String occupation,officeadd,officephone;
    private String username = session.getUsername();  
    
    Response res;   
    Insurance_Client client;
    Collection<OccupationMaster> oms;
    GenericType<Collection<OccupationMaster>> goms;

    public occupation_bean() 
    {
        System.out.println("Hello Occupation Bean ");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        String token="";

        token = request.getSession().getAttribute("token").toString();
       System.out.println("Token="+token);
        
      
        client = new Insurance_Client(token);
      
        oms = new ArrayList<OccupationMaster>();
        goms = new GenericType<Collection<OccupationMaster>>(){};
    }
    
    
    public Collection<OccupationMaster> DisplayOccupation()
    {
        res = client.showAllFamilyDetails(Response.class);
        oms = res.readEntity(goms);
        return oms;
    }
    
    public void insertOccupationDetails()
    {        
        client.addOccupation(occupation, officeadd, officephone, username);
        System.err.println("Insert Occupation...");
    }
    
}
