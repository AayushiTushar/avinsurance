/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import entity.City;
import entity.State;
import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Named(value = "city_state_bean")
@RequestScoped
public class city_state_bean {

    Response res;   
    Insurance_Client client;
    
    Collection<State> states;
    GenericType<Collection<State>> gstates;    
    State currentstate;
    GenericType<State> gcurrentstate;
    
    
    Collection<City> city;
    GenericType<Collection<City>> gcity;    
    City currentcity;
    GenericType<City> gcurrentcity;
    
    private String name;
    private int sid;

    private String cityname;
    private int cityid;

        
    public City getCurrentcity() {
        return currentcity;
    }

    public void setCurrentcity(City currentcity) {
        this.currentcity = currentcity;
    }

    public GenericType<City> getGcurrentcity() {
        return gcurrentcity;
    }

    public void setGcurrentcity(GenericType<City> gcurrentcity) {
        this.gcurrentcity = gcurrentcity;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public int getCityid() {
        return cityid;
    }

    public void setCityid(int cityid) {
        this.cityid = cityid;
    }
   
    public State getCurrentstate() {
        return currentstate;
    }

    public void setCurrentstate(State currentstate) {
        this.currentstate = currentstate;
    }

    public GenericType<State> getGcurrentstate() {
        return gcurrentstate;
    }

    public void setGcurrentstate(GenericType<State> gcurrentstate) {
        this.gcurrentstate = gcurrentstate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

 
    public city_state_bean() 
    {
        System.out.println("Hello state Bean ");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		String token="";

       token = request.getSession().getAttribute("token").toString();
       System.out.println("Token="+token);

        client = new Insurance_Client(token);
           
        states = new ArrayList<State>();
        gstates = new GenericType<Collection<State>>(){};
        
        currentstate = new State();
        gcurrentstate = new GenericType<State>(){};
        
        city = new ArrayList<City>();
        gcity = new GenericType<Collection<City>>(){};
        
        currentcity = new City();
        gcurrentcity = new GenericType<City>(){};
    }
    
    
    public Collection<State> getState()
    {        
        res = client.showAllState(Response.class);
        states = res.readEntity(gstates);
        return states;
    }
    
    
     public String insertState(){
        
         System.out.println("state id =" + getSid());
        if(getSid()>0)
        {
            System.err.println("Called state update");
            
            System.err.println("" + this.sid);
            client.updateState(String.valueOf(sid), name);
            return "State.jsf?faces-redirect=true";
            
        }
        else
        {
            
            System.err.println("Called State insert");
            client.addState(name);
            return "State.jsf?faces-redirect=true";
            
        }
    }
     
     public String removeState(){
       
        System.err.println("delete state");
        client.deleteState(String.valueOf(sid));
        return "State.jsf?faces-redirect=true";
        
    }
     
    public String dataOfState(int ssid)
    {        
        res = client.getState(Response.class,ssid+"");
        currentstate = res.readEntity(gcurrentstate);
        this.name =  currentstate.getStateName();
        this.sid = currentstate.getStateId();

        return "StateForm.jsf";
        
        
    }
    
    
    // city ...


    public Collection<City> getCity()
    {        
        res = client.showAllCity(Response.class);
        city = res.readEntity(gcity);
        return city;
    }


     public String insertCity(){

         System.out.println("city id =" + getCityid());
        if(getCityid()>0)
        {
            System.err.println("Called city update");

            System.err.println("" + this.cityid);
            client.updateCity(String.valueOf(cityid),cityname,String.valueOf(sid));
            return "City.jsf?faces-redirect=true";

        }
        else
        {

            System.err.println("Called city insert");
            client.addCity(cityname,String.valueOf(sid));
            return "City.jsf?faces-redirect=true";

        }
    }

     public String removeCity(){

        System.err.println("delete city");
        client.deleteCity(String.valueOf(cityid));
        return "City.jsf?faces-redirect=true";

    }

    public String dataOfCity(int cid)
    {
        System.err.println("hey ............");
        System.err.println("city id = "+ cid);
        res = client.getCity(Response.class,cid+"");
        currentcity = res.readEntity(gcurrentcity);
        this.cityname = currentcity.getCityName();
        this.sid = currentcity.getStateId().getStateId();
        this.cityid = currentcity.getCityId();
        System.err.println(cityname);
        System.err.println(sid);
        System.err.println(cityid);
        return "CityForm.jsf";


    }

    
    
}
