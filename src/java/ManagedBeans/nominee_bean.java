/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import entity.Nominee;
import helper.session;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;


@Named(value = "nominee_bean")
@RequestScoped
public class nominee_bean {

    private static final String FolderPath = "F:\\Practical_SEM-8\\Insurance_Project\\AVInsurance_Project\\web\\Images\\Users\\";
    
    private String fname,lname,mname;
    private String username = session.getUsername();
    private String occupation,officeadd,officephone;
    private Part lcimg,marksheetimg;
    private int relationid;        
    
    Response res;   
    Insurance_Client client;
    Collection<Nominee> nds;
    GenericType<Collection<Nominee>> gnds;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOfficeadd() {
        return officeadd;
    }

    public void setOfficeadd(String officeadd) {
        this.officeadd = officeadd;
    }

    public String getOfficephone() {
        return officephone;
    }

    public void setOfficephone(String officephone) {
        this.officephone = officephone;
    }

    public Part getLcimg() {
        return lcimg;
    }

    public void setLcimg(Part lcimg) {
        this.lcimg = lcimg;
    }

    public Part getMarksheetimg() {
        return marksheetimg;
    }

    public void setMarksheetimg(Part marksheetimg) {
        this.marksheetimg = marksheetimg;
    }

    public int getRelationid() {
        return relationid;
    }

    public void setRelationid(int relationid) {
        this.relationid = relationid;
    }
    
    
    public nominee_bean() 
    {
        System.out.println("Hello Nominee Bean ");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        String token="";

        token = request.getSession().getAttribute("token").toString();
       System.out.println("Token="+token);
        
      
        client = new Insurance_Client(token);
      
        nds = new ArrayList<Nominee>();
        gnds = new GenericType<Collection<Nominee>>(){};
    }
    
    public void insertNominee()
    {
        String LcImage = "";
        String MarksheetImage = "";
        try{
                InputStream input = lcimg.getInputStream(); 
                InputStream input1 = marksheetimg.getInputStream(); 
                Random mRandom = new Random();
                StringBuilder sb = new StringBuilder();
                sb.append(mRandom.nextInt(9) + 1);
                for(int i=0; i < 11; i++){
                    sb.append(mRandom.nextInt(10));
                }
                String tempStr = sb.toString();
                
                LcImage = tempStr + lcimg.getSubmittedFileName();
                MarksheetImage = tempStr + marksheetimg.getSubmittedFileName();
                System.err.println("Lc name = "+ LcImage); 
                System.err.println("Marksheet name = "+ MarksheetImage); 
                
                Files.copy(input, new File(FolderPath+ "" + LcImage).toPath());
                Files.copy(input1, new File(FolderPath+ "" + MarksheetImage).toPath());
        }
        catch(Exception ex)
        {
            System.err.println("Exception while file upload " + ex.toString());
        }
        
        client.addNominee(fname, mname, lname, occupation, officeadd, officephone, LcImage, MarksheetImage, username,String.valueOf(relationid));
    }
    
    
    public Collection<Nominee> showDetails()
    {
        res = client.showAllNominee(Response.class);
        nds = res.readEntity(gnds);
        return nds;
    }
}
