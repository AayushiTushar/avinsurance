/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import Client.Insurance_Client;
import SeessionBean.InsuranceLocal;
import entity.InsuranceType;
import java.util.ArrayList;
import java.util.Collection;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Named(value = "insurance_bean")
@RequestScoped
public class insurance_bean {

    Response res;   
    Insurance_Client client;
    Collection<InsuranceType> ins;
    GenericType<Collection<InsuranceType>> gins;
    
    InsuranceType currentIns;
    GenericType<InsuranceType> gCurrIns;
    
    private String name,description;
    private int insuranceid;

    public InsuranceType getCurrentIns() {
        return currentIns;
    }

    public void setCurrentIns(InsuranceType currentIns) {
        this.currentIns = currentIns;
    }

    public GenericType<InsuranceType> getgCurrIns() {
        return gCurrIns;
    }

    public void setgCurrIns(GenericType<InsuranceType> gCurrIns) {
        this.gCurrIns = gCurrIns;
    }
        
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getInsuranceid() {
        return insuranceid;
    }

    public void setInsuranceid(int insuranceid) {
        this.insuranceid = insuranceid;
    }
    
    
    
    public insurance_bean() 
    {
        
        System.out.println("Hello Insurance Bean ");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		String token="";

        token = request.getSession().getAttribute("token").toString();
       System.out.println("Token="+token);
        
        //String token1 = request.getHeader("Authorization").substring("Bearer ".length());
      // System.out.println("Token="+token1);
        client = new Insurance_Client(token);
           
      // brc = new BookRestClient(username, password);
        ins = new ArrayList<InsuranceType>();
        gins = new GenericType<Collection<InsuranceType>>(){};
        
        currentIns = new InsuranceType();
        gCurrIns = new GenericType<InsuranceType>(){};
        
    }
    
    
    public Collection<InsuranceType> GetInsurance()
    {        
        res = client.showAllInsurance(Response.class);
        ins = res.readEntity(gins);
        return ins;
    }
    
    
     public String InsertInsurace(){
        
         System.out.println("Insurance id =" + getInsuranceid());
        if(getInsuranceid()>0)
        {
            System.err.println("Called insurance update");
            
            System.err.println("" + this.insuranceid);
            client.updateInsurance(String.valueOf(insuranceid), name, description);
            return "InsuranceType.jsf?faces-redirect=true";
            
        }
        else
        {
            
            System.err.println("Called insurance insert");
            client.addInsurance(name, description);
            return "InsuranceType.jsf?faces-redirect=true";
            
        }
    }
     
     public String RemoveInsurace(){
       
        System.err.println("Called insurance");
        client.deleteInsurance(String.valueOf(insuranceid));
        return "InsuranceType.jsf?faces-redirect=true";
        
    }
     
    public String GetDetail(int ins_id)
    {
        res = client.getInsurance(Response.class,ins_id+"");
        currentIns = res.readEntity(gCurrIns);
        this.name =  currentIns.getInsuranceName();
        this.description = currentIns.getDescription();
        this.insuranceid = currentIns.getInsuranceId();

        
        return "InsuranceForm.jsf";
        
    }
}
