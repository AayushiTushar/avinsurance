/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "premium_details")
@NamedQueries({
    @NamedQuery(name = "PremiumDetails.findAll", query = "SELECT p FROM PremiumDetails p"),
    @NamedQuery(name = "PremiumDetails.findByPremiumId", query = "SELECT p FROM PremiumDetails p WHERE p.premiumId = :premiumId"),
    @NamedQuery(name = "PremiumDetails.findByPaidDate", query = "SELECT p FROM PremiumDetails p WHERE p.paidDate = :paidDate"),
    @NamedQuery(name = "PremiumDetails.findByPaidAmount", query = "SELECT p FROM PremiumDetails p WHERE p.paidAmount = :paidAmount"),
    @NamedQuery(name = "PremiumDetails.findByRecieptNumber", query = "SELECT p FROM PremiumDetails p WHERE p.recieptNumber = :recieptNumber"),
    @NamedQuery(name = "PremiumDetails.findByIsDelete", query = "SELECT p FROM PremiumDetails p WHERE p.isDelete = :isDelete"),
    @NamedQuery(name = "PremiumDetails.findByIsActive", query = "SELECT p FROM PremiumDetails p WHERE p.isActive = :isActive"),
    @NamedQuery(name = "PremiumDetails.findByAddedOn", query = "SELECT p FROM PremiumDetails p WHERE p.addedOn = :addedOn"),
    @NamedQuery(name = "PremiumDetails.findByUpdatedOn", query = "SELECT p FROM PremiumDetails p WHERE p.updatedOn = :updatedOn")})
public class PremiumDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PremiumId")
    private Integer premiumId;
    @Basic(optional = false)
    @Column(name = "PaidDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date paidDate;
    @Basic(optional = false)
    @Column(name = "PaidAmount")
    private float paidAmount;
    @Basic(optional = false)
    @Column(name = "RecieptNumber")
    private String recieptNumber;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "PolicyId", referencedColumnName = "PolicyId")
    @ManyToOne(optional = false)
    private PolicyDetails policyId;
    @JoinColumn(name = "Username", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private PersonalDetails username;

    public PremiumDetails() {
    }

    public PremiumDetails(Integer premiumId) {
        this.premiumId = premiumId;
    }

    public PremiumDetails(Integer premiumId, Date paidDate, float paidAmount, String recieptNumber, Date addedOn) {
        this.premiumId = premiumId;
        this.paidDate = paidDate;
        this.paidAmount = paidAmount;
        this.recieptNumber = recieptNumber;
        this.addedOn = addedOn;
    }

    public Integer getPremiumId() {
        return premiumId;
    }

    public void setPremiumId(Integer premiumId) {
        this.premiumId = premiumId;
    }

    public Date getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Date paidDate) {
        this.paidDate = paidDate;
    }

    public float getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(float paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getRecieptNumber() {
        return recieptNumber;
    }

    public void setRecieptNumber(String recieptNumber) {
        this.recieptNumber = recieptNumber;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public PolicyDetails getPolicyId() {
        return policyId;
    }

    public void setPolicyId(PolicyDetails policyId) {
        this.policyId = policyId;
    }

    public PersonalDetails getUsername() {
        return username;
    }

    public void setUsername(PersonalDetails username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (premiumId != null ? premiumId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PremiumDetails)) {
            return false;
        }
        PremiumDetails other = (PremiumDetails) object;
        if ((this.premiumId == null && other.premiumId != null) || (this.premiumId != null && !this.premiumId.equals(other.premiumId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PremiumDetails[ premiumId=" + premiumId + " ]";
    }
    
}
