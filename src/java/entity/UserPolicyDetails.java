/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "user_policy_details")
@NamedQueries({
    @NamedQuery(name = "UserPolicyDetails.findAll", query = "SELECT u FROM UserPolicyDetails u"),
    @NamedQuery(name = "UserPolicyDetails.findByUserPolicyId", query = "SELECT u FROM UserPolicyDetails u WHERE u.userPolicyId = :userPolicyId"),
    @NamedQuery(name = "UserPolicyDetails.findByPremium", query = "SELECT u FROM UserPolicyDetails u WHERE u.premium = :premium"),
    @NamedQuery(name = "UserPolicyDetails.findByPolicyStartDate", query = "SELECT u FROM UserPolicyDetails u WHERE u.policyStartDate = :policyStartDate"),
    @NamedQuery(name = "UserPolicyDetails.findByTerm", query = "SELECT u FROM UserPolicyDetails u WHERE u.term = :term"),
    @NamedQuery(name = "UserPolicyDetails.findByExpectedEndDate", query = "SELECT u FROM UserPolicyDetails u WHERE u.expectedEndDate = :expectedEndDate"),
    @NamedQuery(name = "UserPolicyDetails.findByExpectedMatureAmount", query = "SELECT u FROM UserPolicyDetails u WHERE u.expectedMatureAmount = :expectedMatureAmount"),
    @NamedQuery(name = "UserPolicyDetails.findByPolicyCertificateNumber", query = "SELECT u FROM UserPolicyDetails u WHERE u.policyCertificateNumber = :policyCertificateNumber"),
    @NamedQuery(name = "UserPolicyDetails.findByStatus", query = "SELECT u FROM UserPolicyDetails u WHERE u.status = :status"),
    @NamedQuery(name = "UserPolicyDetails.findByIsDelete", query = "SELECT u FROM UserPolicyDetails u WHERE u.isDelete = :isDelete"),
    @NamedQuery(name = "UserPolicyDetails.findByIsActive", query = "SELECT u FROM UserPolicyDetails u WHERE u.isActive = :isActive"),
    @NamedQuery(name = "UserPolicyDetails.findByAddedOn", query = "SELECT u FROM UserPolicyDetails u WHERE u.addedOn = :addedOn"),
    @NamedQuery(name = "UserPolicyDetails.findByUpdatedOn", query = "SELECT u FROM UserPolicyDetails u WHERE u.updatedOn = :updatedOn")})
public class UserPolicyDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "UserPolicyId")
    private Integer userPolicyId;
    @Basic(optional = false)
    @Column(name = "Premium")
    private String premium;
    @Basic(optional = false)
    @Column(name = "PolicyStartDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date policyStartDate;
    @Basic(optional = false)
    @Column(name = "Term")
    private int term;
    @Basic(optional = false)
    @Column(name = "ExpectedEndDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expectedEndDate;
    @Basic(optional = false)
    @Column(name = "ExpectedMatureAmount")
    private float expectedMatureAmount;
    @Basic(optional = false)
    @Column(name = "PolicyCertificateNumber")
    private String policyCertificateNumber;
    @Column(name = "Status")
    private Boolean status;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "PolicyId", referencedColumnName = "PolicyId")
    @ManyToOne(optional = false)
    private PolicyDetails policyId;
    @JoinColumn(name = "Username", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private PersonalDetails username;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userPolicyId")
    private Collection<LifeClaim> lifeClaimCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userPolicyId")
    private Collection<VehicleClaim> vehicleClaimCollection;

    public UserPolicyDetails() {
    }

    public UserPolicyDetails(Integer userPolicyId) {
        this.userPolicyId = userPolicyId;
    }

    public UserPolicyDetails(Integer userPolicyId, String premium, Date policyStartDate, int term, Date expectedEndDate, float expectedMatureAmount, String policyCertificateNumber, Date addedOn) {
        this.userPolicyId = userPolicyId;
        this.premium = premium;
        this.policyStartDate = policyStartDate;
        this.term = term;
        this.expectedEndDate = expectedEndDate;
        this.expectedMatureAmount = expectedMatureAmount;
        this.policyCertificateNumber = policyCertificateNumber;
        this.addedOn = addedOn;
    }

    public Integer getUserPolicyId() {
        return userPolicyId;
    }

    public void setUserPolicyId(Integer userPolicyId) {
        this.userPolicyId = userPolicyId;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public Date getPolicyStartDate() {
        return policyStartDate;
    }

    public void setPolicyStartDate(Date policyStartDate) {
        this.policyStartDate = policyStartDate;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public Date getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(Date expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public float getExpectedMatureAmount() {
        return expectedMatureAmount;
    }

    public void setExpectedMatureAmount(float expectedMatureAmount) {
        this.expectedMatureAmount = expectedMatureAmount;
    }

    public String getPolicyCertificateNumber() {
        return policyCertificateNumber;
    }

    public void setPolicyCertificateNumber(String policyCertificateNumber) {
        this.policyCertificateNumber = policyCertificateNumber;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public PolicyDetails getPolicyId() {
        return policyId;
    }

    public void setPolicyId(PolicyDetails policyId) {
        this.policyId = policyId;
    }

    public PersonalDetails getUsername() {
        return username;
    }

    public void setUsername(PersonalDetails username) {
        this.username = username;
    }

    @JsonbTransient
    public Collection<LifeClaim> getLifeClaimCollection() {
        return lifeClaimCollection;
    }

    public void setLifeClaimCollection(Collection<LifeClaim> lifeClaimCollection) {
        this.lifeClaimCollection = lifeClaimCollection;
    }

    @JsonbTransient
    public Collection<VehicleClaim> getVehicleClaimCollection() {
        return vehicleClaimCollection;
    }

    public void setVehicleClaimCollection(Collection<VehicleClaim> vehicleClaimCollection) {
        this.vehicleClaimCollection = vehicleClaimCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userPolicyId != null ? userPolicyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserPolicyDetails)) {
            return false;
        }
        UserPolicyDetails other = (UserPolicyDetails) object;
        if ((this.userPolicyId == null && other.userPolicyId != null) || (this.userPolicyId != null && !this.userPolicyId.equals(other.userPolicyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.UserPolicyDetails[ userPolicyId=" + userPolicyId + " ]";
    }
    
}
