/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "personal_details")
@NamedQueries({
    @NamedQuery(name = "PersonalDetails.findAll", query = "SELECT p FROM PersonalDetails p"),
    @NamedQuery(name = "PersonalDetails.findByFirstName", query = "SELECT p FROM PersonalDetails p WHERE p.firstName = :firstName"),
    @NamedQuery(name = "PersonalDetails.findByMiddleName", query = "SELECT p FROM PersonalDetails p WHERE p.middleName = :middleName"),
    @NamedQuery(name = "PersonalDetails.findByLastName", query = "SELECT p FROM PersonalDetails p WHERE p.lastName = :lastName"),
    @NamedQuery(name = "PersonalDetails.findByGender", query = "SELECT p FROM PersonalDetails p WHERE p.gender = :gender"),
    @NamedQuery(name = "PersonalDetails.findByDob", query = "SELECT p FROM PersonalDetails p WHERE p.dob = :dob"),
    @NamedQuery(name = "PersonalDetails.findByAge", query = "SELECT p FROM PersonalDetails p WHERE p.age = :age"),
    @NamedQuery(name = "PersonalDetails.findByUsername", query = "SELECT p FROM PersonalDetails p WHERE p.username = :username"),
    @NamedQuery(name = "PersonalDetails.findByEmail", query = "SELECT p FROM PersonalDetails p WHERE p.email = :email"),
    @NamedQuery(name = "PersonalDetails.findByPassword", query = "SELECT p FROM PersonalDetails p WHERE p.password = :password"),
    @NamedQuery(name = "PersonalDetails.findByPhoneNumber", query = "SELECT p FROM PersonalDetails p WHERE p.phoneNumber = :phoneNumber"),
    @NamedQuery(name = "PersonalDetails.findByPinCode", query = "SELECT p FROM PersonalDetails p WHERE p.pinCode = :pinCode"),
    @NamedQuery(name = "PersonalDetails.findByPhotoImage", query = "SELECT p FROM PersonalDetails p WHERE p.photoImage = :photoImage"),
    @NamedQuery(name = "PersonalDetails.findByIsDelete", query = "SELECT p FROM PersonalDetails p WHERE p.isDelete = :isDelete"),
    @NamedQuery(name = "PersonalDetails.findByIsActive", query = "SELECT p FROM PersonalDetails p WHERE p.isActive = :isActive"),
    @NamedQuery(name = "PersonalDetails.findByAddedOn", query = "SELECT p FROM PersonalDetails p WHERE p.addedOn = :addedOn"),
    @NamedQuery(name = "PersonalDetails.findByUpdatedOn", query = "SELECT p FROM PersonalDetails p WHERE p.updatedOn = :updatedOn")})
public class PersonalDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "FirstName")
    private String firstName;
    @Column(name = "MiddleName")
    private String middleName;
    @Basic(optional = false)
    @Column(name = "LastName")
    private String lastName;
    @Lob
    @Column(name = "Address")
    private String address;
    @Column(name = "Gender")
    private String gender;
    @Column(name = "DOB")    
    private String dob;
    @Column(name = "Age")
    private Integer age;
    @Id
    @Basic(optional = false)
    @Column(name = "Username")
    private String username;
    @Basic(optional = false)
    @Column(name = "Email")
    private String email;
    @Basic(optional = false)
    @Column(name = "Password")
    private String password;
    @Column(name = "PhoneNumber")
    private String phoneNumber;
    @Column(name = "PinCode")
    private Integer pinCode;
    @Column(name = "PhotoImage")
    private String photoImage;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<OccupationMaster> occupationMasterCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<UserPolicyDetails> userPolicyDetailsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<BankDetails> bankDetailsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<IdProof> idProofCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<Wishlist> wishlistCollection;
    @JoinColumn(name = "CityId", referencedColumnName = "CityId")
    @ManyToOne
    private City cityId;
    @JoinColumn(name = "StateId", referencedColumnName = "StateId")
    @ManyToOne
    private State stateId;
    @JoinColumn(name = "RoleId", referencedColumnName = "RoleId")
    @ManyToOne(optional = false)
    private UserRole roleId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<EducationDetails> educationDetailsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<FamilyDetails> familyDetailsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<HealthInfo> healthInfoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<Nominee> nomineeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<PremiumDetails> premiumDetailsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<VehicleDetails> vehicleDetailsCollection;

    public PersonalDetails() {
    }

    public PersonalDetails(String username) {
        this.username = username;
    }

    public PersonalDetails(String username, String firstName, String lastName, String email, String password, Date addedOn) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.addedOn = addedOn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getPinCode() {
        return pinCode;
    }

    public void setPinCode(Integer pinCode) {
        this.pinCode = pinCode;
    }

    public String getPhotoImage() {
        return photoImage;
    }

    public void setPhotoImage(String photoImage) {
        this.photoImage = photoImage;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @JsonbTransient
    public Collection<OccupationMaster> getOccupationMasterCollection() {
        return occupationMasterCollection;
    }

    public void setOccupationMasterCollection(Collection<OccupationMaster> occupationMasterCollection) {
        this.occupationMasterCollection = occupationMasterCollection;
    }

    @JsonbTransient
    public Collection<UserPolicyDetails> getUserPolicyDetailsCollection() {
        return userPolicyDetailsCollection;
    }

    public void setUserPolicyDetailsCollection(Collection<UserPolicyDetails> userPolicyDetailsCollection) {
        this.userPolicyDetailsCollection = userPolicyDetailsCollection;
    }

    @JsonbTransient
    public Collection<BankDetails> getBankDetailsCollection() {
        return bankDetailsCollection;
    }

    public void setBankDetailsCollection(Collection<BankDetails> bankDetailsCollection) {
        this.bankDetailsCollection = bankDetailsCollection;
    }

    @JsonbTransient
    public Collection<IdProof> getIdProofCollection() {
        return idProofCollection;
    }

    public void setIdProofCollection(Collection<IdProof> idProofCollection) {
        this.idProofCollection = idProofCollection;
    }

    @JsonbTransient
    public Collection<Wishlist> getWishlistCollection() {
        return wishlistCollection;
    }

    public void setWishlistCollection(Collection<Wishlist> wishlistCollection) {
        this.wishlistCollection = wishlistCollection;
    }

    public City getCityId() {
        return cityId;
    }

    public void setCityId(City cityId) {
        this.cityId = cityId;
    }

    public State getStateId() {
        return stateId;
    }

    public void setStateId(State stateId) {
        this.stateId = stateId;
    }

    public UserRole getRoleId() {
        return roleId;
    }

    public void setRoleId(UserRole roleId) {
        this.roleId = roleId;
    }

    @JsonbTransient
    public Collection<EducationDetails> getEducationDetailsCollection() {
        return educationDetailsCollection;
    }

    public void setEducationDetailsCollection(Collection<EducationDetails> educationDetailsCollection) {
        this.educationDetailsCollection = educationDetailsCollection;
    }

    @JsonbTransient
    public Collection<FamilyDetails> getFamilyDetailsCollection() {
        return familyDetailsCollection;
    }

    public void setFamilyDetailsCollection(Collection<FamilyDetails> familyDetailsCollection) {
        this.familyDetailsCollection = familyDetailsCollection;
    }

    @JsonbTransient
    public Collection<HealthInfo> getHealthInfoCollection() {
        return healthInfoCollection;
    }

    public void setHealthInfoCollection(Collection<HealthInfo> healthInfoCollection) {
        this.healthInfoCollection = healthInfoCollection;
    }

    @JsonbTransient
    public Collection<Nominee> getNomineeCollection() {
        return nomineeCollection;
    }

    public void setNomineeCollection(Collection<Nominee> nomineeCollection) {
        this.nomineeCollection = nomineeCollection;
    }

    @JsonbTransient
    public Collection<PremiumDetails> getPremiumDetailsCollection() {
        return premiumDetailsCollection;
    }

    public void setPremiumDetailsCollection(Collection<PremiumDetails> premiumDetailsCollection) {
        this.premiumDetailsCollection = premiumDetailsCollection;
    }

    @JsonbTransient
    public Collection<VehicleDetails> getVehicleDetailsCollection() {
        return vehicleDetailsCollection;
    }

    public void setVehicleDetailsCollection(Collection<VehicleDetails> vehicleDetailsCollection) {
        this.vehicleDetailsCollection = vehicleDetailsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (username != null ? username.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonalDetails)) {
            return false;
        }
        PersonalDetails other = (PersonalDetails) object;
        if ((this.username == null && other.username != null) || (this.username != null && !this.username.equals(other.username))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PersonalDetails[ username=" + username + " ]";
    }

    
}
