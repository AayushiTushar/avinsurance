/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "vehicle_details")
@NamedQueries({
    @NamedQuery(name = "VehicleDetails.findAll", query = "SELECT v FROM VehicleDetails v"),
    @NamedQuery(name = "VehicleDetails.findByVehicleDetailId", query = "SELECT v FROM VehicleDetails v WHERE v.vehicleDetailId = :vehicleDetailId"),
    @NamedQuery(name = "VehicleDetails.findByModelNumber", query = "SELECT v FROM VehicleDetails v WHERE v.modelNumber = :modelNumber"),
    @NamedQuery(name = "VehicleDetails.findByRCBookNumber", query = "SELECT v FROM VehicleDetails v WHERE v.rCBookNumber = :rCBookNumber"),
    @NamedQuery(name = "VehicleDetails.findByNumberPlate", query = "SELECT v FROM VehicleDetails v WHERE v.numberPlate = :numberPlate"),
    @NamedQuery(name = "VehicleDetails.findByChesiesNumber", query = "SELECT v FROM VehicleDetails v WHERE v.chesiesNumber = :chesiesNumber"),
    @NamedQuery(name = "VehicleDetails.findByBodyStyle", query = "SELECT v FROM VehicleDetails v WHERE v.bodyStyle = :bodyStyle"),
    @NamedQuery(name = "VehicleDetails.findByBoughtYear", query = "SELECT v FROM VehicleDetails v WHERE v.boughtYear = :boughtYear"),
    @NamedQuery(name = "VehicleDetails.findByRCBookImage", query = "SELECT v FROM VehicleDetails v WHERE v.rCBookImage = :rCBookImage"),
    @NamedQuery(name = "VehicleDetails.findByLicenceImage", query = "SELECT v FROM VehicleDetails v WHERE v.licenceImage = :licenceImage"),
    @NamedQuery(name = "VehicleDetails.findByIsDelete", query = "SELECT v FROM VehicleDetails v WHERE v.isDelete = :isDelete"),
    @NamedQuery(name = "VehicleDetails.findByIsActive", query = "SELECT v FROM VehicleDetails v WHERE v.isActive = :isActive"),
    @NamedQuery(name = "VehicleDetails.findByAddedOn", query = "SELECT v FROM VehicleDetails v WHERE v.addedOn = :addedOn"),
    @NamedQuery(name = "VehicleDetails.findByUpdatedOn", query = "SELECT v FROM VehicleDetails v WHERE v.updatedOn = :updatedOn")})
public class VehicleDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "VehicleDetailId")
    private Integer vehicleDetailId;
    @Basic(optional = false)
    @Column(name = "ModelNumber")
    private String modelNumber;
    @Basic(optional = false)
    @Column(name = "RCBookNumber")
    private String rCBookNumber;
    @Basic(optional = false)
    @Column(name = "NumberPlate")
    private String numberPlate;
    @Basic(optional = false)
    @Column(name = "ChesiesNumber")
    private String chesiesNumber;
    @Basic(optional = false)
    @Column(name = "BodyStyle")
    private String bodyStyle;
    @Basic(optional = false)
    @Column(name = "BoughtYear")    
    private String boughtYear;
    @Basic(optional = false)
    @Column(name = "RCBookImage")
    private String rCBookImage;
    @Basic(optional = false)
    @Column(name = "LicenceImage")
    private String licenceImage;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "PolicyId", referencedColumnName = "PolicyId")
    @ManyToOne(optional = false)
    private PolicyDetails policyId;
    @JoinColumn(name = "Username", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private PersonalDetails username;

    public VehicleDetails() {
    }

    public VehicleDetails(Integer vehicleDetailId) {
        this.vehicleDetailId = vehicleDetailId;
    }

    public VehicleDetails(Integer vehicleDetailId, String modelNumber, String rCBookNumber, String numberPlate, String chesiesNumber, String bodyStyle, String boughtYear, String rCBookImage, String licenceImage, Date addedOn) {
        this.vehicleDetailId = vehicleDetailId;
        this.modelNumber = modelNumber;
        this.rCBookNumber = rCBookNumber;
        this.numberPlate = numberPlate;
        this.chesiesNumber = chesiesNumber;
        this.bodyStyle = bodyStyle;
        this.boughtYear = boughtYear;
        this.rCBookImage = rCBookImage;
        this.licenceImage = licenceImage;
        this.addedOn = addedOn;
    }

    public Integer getVehicleDetailId() {
        return vehicleDetailId;
    }

    public void setVehicleDetailId(Integer vehicleDetailId) {
        this.vehicleDetailId = vehicleDetailId;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getRCBookNumber() {
        return rCBookNumber;
    }

    public void setRCBookNumber(String rCBookNumber) {
        this.rCBookNumber = rCBookNumber;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public String getChesiesNumber() {
        return chesiesNumber;
    }

    public void setChesiesNumber(String chesiesNumber) {
        this.chesiesNumber = chesiesNumber;
    }

    public String getBodyStyle() {
        return bodyStyle;
    }

    public void setBodyStyle(String bodyStyle) {
        this.bodyStyle = bodyStyle;
    }

    public String getBoughtYear() {
        return boughtYear;
    }

    public void setBoughtYear(String boughtYear) {
        this.boughtYear = boughtYear;
    }

    public String getRCBookImage() {
        return rCBookImage;
    }

    public void setRCBookImage(String rCBookImage) {
        this.rCBookImage = rCBookImage;
    }

    public String getLicenceImage() {
        return licenceImage;
    }

    public void setLicenceImage(String licenceImage) {
        this.licenceImage = licenceImage;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public PolicyDetails getPolicyId() {
        return policyId;
    }

    public void setPolicyId(PolicyDetails policyId) {
        this.policyId = policyId;
    }

    public PersonalDetails getUsername() {
        return username;
    }

    public void setUsername(PersonalDetails username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehicleDetailId != null ? vehicleDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleDetails)) {
            return false;
        }
        VehicleDetails other = (VehicleDetails) object;
        if ((this.vehicleDetailId == null && other.vehicleDetailId != null) || (this.vehicleDetailId != null && !this.vehicleDetailId.equals(other.vehicleDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.VehicleDetails[ vehicleDetailId=" + vehicleDetailId + " ]";
    }
    
}
