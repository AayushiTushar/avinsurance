/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "bank_details")
@NamedQueries({
    @NamedQuery(name = "BankDetails.findAll", query = "SELECT b FROM BankDetails b"),
    @NamedQuery(name = "BankDetails.findByUserBankId", query = "SELECT b FROM BankDetails b WHERE b.userBankId = :userBankId"),
    @NamedQuery(name = "BankDetails.findByBankName", query = "SELECT b FROM BankDetails b WHERE b.bankName = :bankName"),
    @NamedQuery(name = "BankDetails.findByAccountType", query = "SELECT b FROM BankDetails b WHERE b.accountType = :accountType"),
    @NamedQuery(name = "BankDetails.findByAccountNumber", query = "SELECT b FROM BankDetails b WHERE b.accountNumber = :accountNumber"),
    @NamedQuery(name = "BankDetails.findByBranch", query = "SELECT b FROM BankDetails b WHERE b.branch = :branch"),
    @NamedQuery(name = "BankDetails.findByIFSCCode", query = "SELECT b FROM BankDetails b WHERE b.iFSCCode = :iFSCCode"),
    @NamedQuery(name = "BankDetails.findByHolderName", query = "SELECT b FROM BankDetails b WHERE b.holderName = :holderName"),
    @NamedQuery(name = "BankDetails.findByPassbookImage", query = "SELECT b FROM BankDetails b WHERE b.passbookImage = :passbookImage"),
    @NamedQuery(name = "BankDetails.findByIsDelete", query = "SELECT b FROM BankDetails b WHERE b.isDelete = :isDelete"),
    @NamedQuery(name = "BankDetails.findByIsActive", query = "SELECT b FROM BankDetails b WHERE b.isActive = :isActive"),
    @NamedQuery(name = "BankDetails.findByAddedOn", query = "SELECT b FROM BankDetails b WHERE b.addedOn = :addedOn"),
    @NamedQuery(name = "BankDetails.findByUpdatedOn", query = "SELECT b FROM BankDetails b WHERE b.updatedOn = :updatedOn")})
public class BankDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "UserBankId")
    private Integer userBankId;
    @Basic(optional = false)
    @Column(name = "BankName")
    private String bankName;
    @Basic(optional = false)
    @Column(name = "AccountType")
    private String accountType;
    @Basic(optional = false)
    @Column(name = "AccountNumber")
    private long accountNumber;
    @Basic(optional = false)
    @Column(name = "Branch")
    private String branch;
    @Basic(optional = false)
    @Column(name = "IFSCCode")
    private String iFSCCode;
    @Basic(optional = false)
    @Column(name = "HolderName")
    private String holderName;
    @Basic(optional = false)
    @Column(name = "PassbookImage")
    private String passbookImage;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "Username", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private PersonalDetails username;

    public BankDetails() {
    }

    public BankDetails(Integer userBankId) {
        this.userBankId = userBankId;
    }

    public BankDetails(Integer userBankId, String bankName, String accountType, long accountNumber, String branch, String iFSCCode, String holderName, String passbookImage, Date addedOn) {
        this.userBankId = userBankId;
        this.bankName = bankName;
        this.accountType = accountType;
        this.accountNumber = accountNumber;
        this.branch = branch;
        this.iFSCCode = iFSCCode;
        this.holderName = holderName;
        this.passbookImage = passbookImage;
        this.addedOn = addedOn;
    }

    public Integer getUserBankId() {
        return userBankId;
    }

    public void setUserBankId(Integer userBankId) {
        this.userBankId = userBankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getIFSCCode() {
        return iFSCCode;
    }

    public void setIFSCCode(String iFSCCode) {
        this.iFSCCode = iFSCCode;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getPassbookImage() {
        return passbookImage;
    }

    public void setPassbookImage(String passbookImage) {
        this.passbookImage = passbookImage;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public PersonalDetails getUsername() {
        return username;
    }

    public void setUsername(PersonalDetails username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userBankId != null ? userBankId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankDetails)) {
            return false;
        }
        BankDetails other = (BankDetails) object;
        if ((this.userBankId == null && other.userBankId != null) || (this.userBankId != null && !this.userBankId.equals(other.userBankId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.BankDetails[ userBankId=" + userBankId + " ]";
    }
    
}
