/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "vehicle_claim")
@NamedQueries({
    @NamedQuery(name = "VehicleClaim.findAll", query = "SELECT v FROM VehicleClaim v"),
    @NamedQuery(name = "VehicleClaim.findByVehicleClaimId", query = "SELECT v FROM VehicleClaim v WHERE v.vehicleClaimId = :vehicleClaimId"),
    @NamedQuery(name = "VehicleClaim.findByVerifiedBy", query = "SELECT v FROM VehicleClaim v WHERE v.verifiedBy = :verifiedBy"),
    @NamedQuery(name = "VehicleClaim.findByStatus", query = "SELECT v FROM VehicleClaim v WHERE v.status = :status"),
    @NamedQuery(name = "VehicleClaim.findByVehicleBill", query = "SELECT v FROM VehicleClaim v WHERE v.vehicleBill = :vehicleBill"),
    @NamedQuery(name = "VehicleClaim.findByIsDelete", query = "SELECT v FROM VehicleClaim v WHERE v.isDelete = :isDelete"),
    @NamedQuery(name = "VehicleClaim.findByIsActive", query = "SELECT v FROM VehicleClaim v WHERE v.isActive = :isActive"),
    @NamedQuery(name = "VehicleClaim.findByAddedOn", query = "SELECT v FROM VehicleClaim v WHERE v.addedOn = :addedOn"),
    @NamedQuery(name = "VehicleClaim.findByUpdatedOn", query = "SELECT v FROM VehicleClaim v WHERE v.updatedOn = :updatedOn")})
public class VehicleClaim implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "VehicleClaimId")
    private Integer vehicleClaimId;
    @Lob
    @Column(name = "Description")
    private String description;
    @Column(name = "VerifiedBy")
    private String verifiedBy;
    @Column(name = "Status")
    private String status;
    @Column(name = "VehicleBill")
    private String vehicleBill;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicleClaimId")
    private Collection<VehicleImage> vehicleImageCollection;
    @JoinColumn(name = "UserPolicyId", referencedColumnName = "UserPolicyId")
    @ManyToOne(optional = false)
    private UserPolicyDetails userPolicyId;

    public VehicleClaim() {
    }

    public VehicleClaim(Integer vehicleClaimId) {
        this.vehicleClaimId = vehicleClaimId;
    }

    public VehicleClaim(Integer vehicleClaimId, Date addedOn) {
        this.vehicleClaimId = vehicleClaimId;
        this.addedOn = addedOn;
    }

    public Integer getVehicleClaimId() {
        return vehicleClaimId;
    }

    public void setVehicleClaimId(Integer vehicleClaimId) {
        this.vehicleClaimId = vehicleClaimId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVehicleBill() {
        return vehicleBill;
    }

    public void setVehicleBill(String vehicleBill) {
        this.vehicleBill = vehicleBill;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @JsonbTransient
    public Collection<VehicleImage> getVehicleImageCollection() {
        return vehicleImageCollection;
    }

    public void setVehicleImageCollection(Collection<VehicleImage> vehicleImageCollection) {
        this.vehicleImageCollection = vehicleImageCollection;
    }

    public UserPolicyDetails getUserPolicyId() {
        return userPolicyId;
    }

    public void setUserPolicyId(UserPolicyDetails userPolicyId) {
        this.userPolicyId = userPolicyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehicleClaimId != null ? vehicleClaimId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleClaim)) {
            return false;
        }
        VehicleClaim other = (VehicleClaim) object;
        if ((this.vehicleClaimId == null && other.vehicleClaimId != null) || (this.vehicleClaimId != null && !this.vehicleClaimId.equals(other.vehicleClaimId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.VehicleClaim[ vehicleClaimId=" + vehicleClaimId + " ]";
    }
    
}
