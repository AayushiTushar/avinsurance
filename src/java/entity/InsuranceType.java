/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "insurance_type")
@NamedQueries({
    @NamedQuery(name = "InsuranceType.findAll", query = "SELECT i FROM InsuranceType i"),
    @NamedQuery(name = "InsuranceType.findByInsuranceId", query = "SELECT i FROM InsuranceType i WHERE i.insuranceId = :insuranceId"),
    @NamedQuery(name = "InsuranceType.findByInsuranceName", query = "SELECT i FROM InsuranceType i WHERE i.insuranceName = :insuranceName"),
    @NamedQuery(name = "InsuranceType.findByIsDelete", query = "SELECT i FROM InsuranceType i WHERE i.isDelete = :isDelete"),
    @NamedQuery(name = "InsuranceType.findByIsActive", query = "SELECT i FROM InsuranceType i WHERE i.isActive = :isActive"),
    @NamedQuery(name = "InsuranceType.findByAddedOn", query = "SELECT i FROM InsuranceType i WHERE i.addedOn = :addedOn"),
    @NamedQuery(name = "InsuranceType.findByUpdatedOn", query = "SELECT i FROM InsuranceType i WHERE i.updatedOn = :updatedOn")})
public class InsuranceType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "InsuranceId")
    private Integer insuranceId;
    @Basic(optional = false)
    @Column(name = "InsuranceName")
    private String insuranceName;
    @Lob
    @Column(name = "Description")
    private String description;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insuranceId")
    private Collection<PolicyDetails> policyDetailsCollection;

    public InsuranceType() {
    }

    public InsuranceType(Integer insuranceId) {
        this.insuranceId = insuranceId;
    }

    public InsuranceType(Integer insuranceId, String insuranceName, Date addedOn) {
        this.insuranceId = insuranceId;
        this.insuranceName = insuranceName;
        this.addedOn = addedOn;
    }

    public Integer getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(Integer insuranceId) {
        this.insuranceId = insuranceId;
    }

    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @JsonbTransient
    public Collection<PolicyDetails> getPolicyDetailsCollection() {
        return policyDetailsCollection;
    }

    public void setPolicyDetailsCollection(Collection<PolicyDetails> policyDetailsCollection) {
        this.policyDetailsCollection = policyDetailsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insuranceId != null ? insuranceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InsuranceType)) {
            return false;
        }
        InsuranceType other = (InsuranceType) object;
        if ((this.insuranceId == null && other.insuranceId != null) || (this.insuranceId != null && !this.insuranceId.equals(other.insuranceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.InsuranceType[ insuranceId=" + insuranceId + " ]";
    }
    
}
