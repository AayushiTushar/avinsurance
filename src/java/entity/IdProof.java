/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "id_proof")
@NamedQueries({
    @NamedQuery(name = "IdProof.findAll", query = "SELECT i FROM IdProof i"),
    @NamedQuery(name = "IdProof.findByProofId", query = "SELECT i FROM IdProof i WHERE i.proofId = :proofId"),
    @NamedQuery(name = "IdProof.findByNumber", query = "SELECT i FROM IdProof i WHERE i.number = :number"),
    @NamedQuery(name = "IdProof.findByImage", query = "SELECT i FROM IdProof i WHERE i.image = :image"),
    @NamedQuery(name = "IdProof.findByIsDelete", query = "SELECT i FROM IdProof i WHERE i.isDelete = :isDelete"),
    @NamedQuery(name = "IdProof.findByIsActive", query = "SELECT i FROM IdProof i WHERE i.isActive = :isActive"),
    @NamedQuery(name = "IdProof.findByAddedOn", query = "SELECT i FROM IdProof i WHERE i.addedOn = :addedOn"),
    @NamedQuery(name = "IdProof.findByUpdatedOn", query = "SELECT i FROM IdProof i WHERE i.updatedOn = :updatedOn")})
public class IdProof implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ProofId")
    private Integer proofId;
    @Basic(optional = false)
    @Column(name = "Number")
    private String number;
    @Basic(optional = false)
    @Column(name = "Image")
    private String image;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "MasterProofId", referencedColumnName = "MasterProofId")
    @ManyToOne(optional = false)
    private IdProofMaster masterProofId;
    @JoinColumn(name = "Username", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private PersonalDetails username;

    public IdProof() {
    }

    public IdProof(Integer proofId) {
        this.proofId = proofId;
    }

    public IdProof(Integer proofId, String number, String image, Date addedOn) {
        this.proofId = proofId;
        this.number = number;
        this.image = image;
        this.addedOn = addedOn;
    }

    public Integer getProofId() {
        return proofId;
    }

    public void setProofId(Integer proofId) {
        this.proofId = proofId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public IdProofMaster getMasterProofId() {
        return masterProofId;
    }

    public void setMasterProofId(IdProofMaster masterProofId) {
        this.masterProofId = masterProofId;
    }

    public PersonalDetails getUsername() {
        return username;
    }

    public void setUsername(PersonalDetails username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proofId != null ? proofId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IdProof)) {
            return false;
        }
        IdProof other = (IdProof) object;
        if ((this.proofId == null && other.proofId != null) || (this.proofId != null && !this.proofId.equals(other.proofId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.IdProof[ proofId=" + proofId + " ]";
    }
    
}
