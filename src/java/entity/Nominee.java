/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "nominee")
@NamedQueries({
    @NamedQuery(name = "Nominee.findAll", query = "SELECT n FROM Nominee n"),
    @NamedQuery(name = "Nominee.findByNomineeId", query = "SELECT n FROM Nominee n WHERE n.nomineeId = :nomineeId"),
    @NamedQuery(name = "Nominee.findByFirstName", query = "SELECT n FROM Nominee n WHERE n.firstName = :firstName"),
    @NamedQuery(name = "Nominee.findByMiddleName", query = "SELECT n FROM Nominee n WHERE n.middleName = :middleName"),
    @NamedQuery(name = "Nominee.findByLastName", query = "SELECT n FROM Nominee n WHERE n.lastName = :lastName"),
    @NamedQuery(name = "Nominee.findByOccupation", query = "SELECT n FROM Nominee n WHERE n.occupation = :occupation"),
    @NamedQuery(name = "Nominee.findByOfficeNumber", query = "SELECT n FROM Nominee n WHERE n.officeNumber = :officeNumber"),
    @NamedQuery(name = "Nominee.findByLCImage", query = "SELECT n FROM Nominee n WHERE n.lCImage = :lCImage"),
    @NamedQuery(name = "Nominee.findByMarksheetImage", query = "SELECT n FROM Nominee n WHERE n.marksheetImage = :marksheetImage")})
public class Nominee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "NomineeId")
    private Integer nomineeId;
    @Basic(optional = false)
    @Column(name = "FirstName")
    private String firstName;
    @Basic(optional = false)
    @Column(name = "MiddleName")
    private String middleName;
    @Basic(optional = false)
    @Column(name = "LastName")
    private String lastName;
    @Basic(optional = false)
    @Column(name = "Occupation")
    private String occupation;
    @Basic(optional = false)
    @Lob
    @Column(name = "OfficeAddress")
    private String officeAddress;
    @Basic(optional = false)
    @Column(name = "OfficeNumber")
    private String officeNumber;
    @Basic(optional = false)
    @Column(name = "LCImage")
    private String lCImage;
    @Basic(optional = false)
    @Column(name = "MarksheetImage")
    private String marksheetImage;
    @JoinColumn(name = "Username", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private PersonalDetails username;
    @JoinColumn(name = "RelationId", referencedColumnName = "RelationId")
    @ManyToOne(optional = false)
    private RelationDetails relationId;

    public Nominee() {
    }

    public Nominee(Integer nomineeId) {
        this.nomineeId = nomineeId;
    }

    public Nominee(Integer nomineeId, String firstName, String middleName, String lastName, String occupation, String officeAddress, String officeNumber, String lCImage, String marksheetImage) {
        this.nomineeId = nomineeId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.occupation = occupation;
        this.officeAddress = officeAddress;
        this.officeNumber = officeNumber;
        this.lCImage = lCImage;
        this.marksheetImage = marksheetImage;
    }

    public Integer getNomineeId() {
        return nomineeId;
    }

    public void setNomineeId(Integer nomineeId) {
        this.nomineeId = nomineeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    public String getLCImage() {
        return lCImage;
    }

    public void setLCImage(String lCImage) {
        this.lCImage = lCImage;
    }

    public String getMarksheetImage() {
        return marksheetImage;
    }

    public void setMarksheetImage(String marksheetImage) {
        this.marksheetImage = marksheetImage;
    }

    public PersonalDetails getUsername() {
        return username;
    }

    public void setUsername(PersonalDetails username) {
        this.username = username;
    }

    public RelationDetails getRelationId() {
        return relationId;
    }

    public void setRelationId(RelationDetails relationId) {
        this.relationId = relationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nomineeId != null ? nomineeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nominee)) {
            return false;
        }
        Nominee other = (Nominee) object;
        if ((this.nomineeId == null && other.nomineeId != null) || (this.nomineeId != null && !this.nomineeId.equals(other.nomineeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Nominee[ nomineeId=" + nomineeId + " ]";
    }
    
}
