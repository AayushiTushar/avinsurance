/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "life_claim")
@NamedQueries({
    @NamedQuery(name = "LifeClaim.findAll", query = "SELECT l FROM LifeClaim l"),
    @NamedQuery(name = "LifeClaim.findByLClaimId", query = "SELECT l FROM LifeClaim l WHERE l.lClaimId = :lClaimId"),
    @NamedQuery(name = "LifeClaim.findByDeathCertificate", query = "SELECT l FROM LifeClaim l WHERE l.deathCertificate = :deathCertificate"),
    @NamedQuery(name = "LifeClaim.findByDateOfDeath", query = "SELECT l FROM LifeClaim l WHERE l.dateOfDeath = :dateOfDeath"),
    @NamedQuery(name = "LifeClaim.findByVerifiedBy", query = "SELECT l FROM LifeClaim l WHERE l.verifiedBy = :verifiedBy"),
    @NamedQuery(name = "LifeClaim.findByStatus", query = "SELECT l FROM LifeClaim l WHERE l.status = :status"),
    @NamedQuery(name = "LifeClaim.findByIsDelete", query = "SELECT l FROM LifeClaim l WHERE l.isDelete = :isDelete"),
    @NamedQuery(name = "LifeClaim.findByIsActive", query = "SELECT l FROM LifeClaim l WHERE l.isActive = :isActive"),
    @NamedQuery(name = "LifeClaim.findByAddedOn", query = "SELECT l FROM LifeClaim l WHERE l.addedOn = :addedOn"),
    @NamedQuery(name = "LifeClaim.findByUpdatedOn", query = "SELECT l FROM LifeClaim l WHERE l.updatedOn = :updatedOn")})
public class LifeClaim implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "LClaimId")
    private Integer lClaimId;
    @Basic(optional = false)
    @Column(name = "DeathCertificate")
    private String deathCertificate;
    @Basic(optional = false)
    @Column(name = "DateOfDeath")   
    private String dateOfDeath;
    @Column(name = "VerifiedBy")
    private String verifiedBy;
    @Column(name = "Status")
    private String status;
    @Lob
    @Column(name = "Description")
    private String description;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "UserPolicyId", referencedColumnName = "UserPolicyId")
    @ManyToOne(optional = false)
    private UserPolicyDetails userPolicyId;

    public LifeClaim() {
    }

    public LifeClaim(Integer lClaimId) {
        this.lClaimId = lClaimId;
    }

    public LifeClaim(Integer lClaimId, String deathCertificate, String dateOfDeath, Date addedOn) {
        this.lClaimId = lClaimId;
        this.deathCertificate = deathCertificate;
        this.dateOfDeath = dateOfDeath;
        this.addedOn = addedOn;
    }

    public Integer getLClaimId() {
        return lClaimId;
    }

    public void setLClaimId(Integer lClaimId) {
        this.lClaimId = lClaimId;
    }

    public String getDeathCertificate() {
        return deathCertificate;
    }

    public void setDeathCertificate(String deathCertificate) {
        this.deathCertificate = deathCertificate;
    }

    public String getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(String dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public UserPolicyDetails getUserPolicyId() {
        return userPolicyId;
    }

    public void setUserPolicyId(UserPolicyDetails userPolicyId) {
        this.userPolicyId = userPolicyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lClaimId != null ? lClaimId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LifeClaim)) {
            return false;
        }
        LifeClaim other = (LifeClaim) object;
        if ((this.lClaimId == null && other.lClaimId != null) || (this.lClaimId != null && !this.lClaimId.equals(other.lClaimId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.LifeClaim[ lClaimId=" + lClaimId + " ]";
    }
    
}
