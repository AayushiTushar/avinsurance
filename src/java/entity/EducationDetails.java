/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "education_details")
@NamedQueries({
    @NamedQuery(name = "EducationDetails.findAll", query = "SELECT e FROM EducationDetails e"),
    @NamedQuery(name = "EducationDetails.findByEducationId", query = "SELECT e FROM EducationDetails e WHERE e.educationId = :educationId"),
    @NamedQuery(name = "EducationDetails.findByLCImage", query = "SELECT e FROM EducationDetails e WHERE e.lCImage = :lCImage"),
    @NamedQuery(name = "EducationDetails.findByMarksheetImage", query = "SELECT e FROM EducationDetails e WHERE e.marksheetImage = :marksheetImage"),
    @NamedQuery(name = "EducationDetails.findByIsDelete", query = "SELECT e FROM EducationDetails e WHERE e.isDelete = :isDelete"),
    @NamedQuery(name = "EducationDetails.findByIsActive", query = "SELECT e FROM EducationDetails e WHERE e.isActive = :isActive"),
    @NamedQuery(name = "EducationDetails.findByAddedOn", query = "SELECT e FROM EducationDetails e WHERE e.addedOn = :addedOn"),
    @NamedQuery(name = "EducationDetails.findByUpdatedOn", query = "SELECT e FROM EducationDetails e WHERE e.updatedOn = :updatedOn")})
public class EducationDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "EducationId")
    private Integer educationId;
    @Basic(optional = false)
    @Column(name = "LCImage")
    private String lCImage;
    @Basic(optional = false)
    @Column(name = "MarksheetImage")
    private String marksheetImage;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "Username", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private PersonalDetails username;

    public EducationDetails() {
    }

    public EducationDetails(Integer educationId) {
        this.educationId = educationId;
    }

    public EducationDetails(Integer educationId, String lCImage, String marksheetImage, Date addedOn) {
        this.educationId = educationId;
        this.lCImage = lCImage;
        this.marksheetImage = marksheetImage;
        this.addedOn = addedOn;
    }

    public Integer getEducationId() {
        return educationId;
    }

    public void setEducationId(Integer educationId) {
        this.educationId = educationId;
    }

    public String getLCImage() {
        return lCImage;
    }

    public void setLCImage(String lCImage) {
        this.lCImage = lCImage;
    }

    public String getMarksheetImage() {
        return marksheetImage;
    }

    public void setMarksheetImage(String marksheetImage) {
        this.marksheetImage = marksheetImage;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public PersonalDetails getUsername() {
        return username;
    }

    public void setUsername(PersonalDetails username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (educationId != null ? educationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EducationDetails)) {
            return false;
        }
        EducationDetails other = (EducationDetails) object;
        if ((this.educationId == null && other.educationId != null) || (this.educationId != null && !this.educationId.equals(other.educationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.EducationDetails[ educationId=" + educationId + " ]";
    }
    
}
