/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "health_info")
@NamedQueries({
    @NamedQuery(name = "HealthInfo.findAll", query = "SELECT h FROM HealthInfo h"),
    @NamedQuery(name = "HealthInfo.findByHealthId", query = "SELECT h FROM HealthInfo h WHERE h.healthId = :healthId"),
    @NamedQuery(name = "HealthInfo.findByDisease", query = "SELECT h FROM HealthInfo h WHERE h.disease = :disease"),
    @NamedQuery(name = "HealthInfo.findByDocument", query = "SELECT h FROM HealthInfo h WHERE h.document = :document"),
    @NamedQuery(name = "HealthInfo.findByIsDelete", query = "SELECT h FROM HealthInfo h WHERE h.isDelete = :isDelete"),
    @NamedQuery(name = "HealthInfo.findByIsActive", query = "SELECT h FROM HealthInfo h WHERE h.isActive = :isActive"),
    @NamedQuery(name = "HealthInfo.findByAddedOn", query = "SELECT h FROM HealthInfo h WHERE h.addedOn = :addedOn"),
    @NamedQuery(name = "HealthInfo.findByUpdatedOn", query = "SELECT h FROM HealthInfo h WHERE h.updatedOn = :updatedOn")})
public class HealthInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "HealthId")
    private Integer healthId;
    @Basic(optional = false)
    @Column(name = "Disease")
    private String disease;
    @Basic(optional = false)
    @Lob
    @Column(name = "Description")
    private String description;
    @Basic(optional = false)
    @Column(name = "Document")
    private String document;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "Username", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private PersonalDetails username;

    public HealthInfo() {
    }

    public HealthInfo(Integer healthId) {
        this.healthId = healthId;
    }

    public HealthInfo(Integer healthId, String disease, String description, String document, Date addedOn) {
        this.healthId = healthId;
        this.disease = disease;
        this.description = description;
        this.document = document;
        this.addedOn = addedOn;
    }

    public Integer getHealthId() {
        return healthId;
    }

    public void setHealthId(Integer healthId) {
        this.healthId = healthId;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public PersonalDetails getUsername() {
        return username;
    }

    public void setUsername(PersonalDetails username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (healthId != null ? healthId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HealthInfo)) {
            return false;
        }
        HealthInfo other = (HealthInfo) object;
        if ((this.healthId == null && other.healthId != null) || (this.healthId != null && !this.healthId.equals(other.healthId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.HealthInfo[ healthId=" + healthId + " ]";
    }
    
}
