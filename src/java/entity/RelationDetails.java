/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "relation_details")
@NamedQueries({
    @NamedQuery(name = "RelationDetails.findAll", query = "SELECT r FROM RelationDetails r"),
    @NamedQuery(name = "RelationDetails.findByRelationId", query = "SELECT r FROM RelationDetails r WHERE r.relationId = :relationId"),
    @NamedQuery(name = "RelationDetails.findByRelation", query = "SELECT r FROM RelationDetails r WHERE r.relation = :relation"),
    @NamedQuery(name = "RelationDetails.findByIsDelete", query = "SELECT r FROM RelationDetails r WHERE r.isDelete = :isDelete"),
    @NamedQuery(name = "RelationDetails.findByIsActive", query = "SELECT r FROM RelationDetails r WHERE r.isActive = :isActive"),
    @NamedQuery(name = "RelationDetails.findByAddedOn", query = "SELECT r FROM RelationDetails r WHERE r.addedOn = :addedOn"),
    @NamedQuery(name = "RelationDetails.findByUpdatedOn", query = "SELECT r FROM RelationDetails r WHERE r.updatedOn = :updatedOn")})
public class RelationDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RelationId")
    private Integer relationId;
    @Basic(optional = false)
    @Column(name = "Relation")
    private String relation;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "relationId")
    private Collection<FamilyDetails> familyDetailsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "relationId")
    private Collection<Nominee> nomineeCollection;

    public RelationDetails() {
    }

    public RelationDetails(Integer relationId) {
        this.relationId = relationId;
    }

    public RelationDetails(Integer relationId, String relation, Date addedOn) {
        this.relationId = relationId;
        this.relation = relation;
        this.addedOn = addedOn;
    }

    public Integer getRelationId() {
        return relationId;
    }

    public void setRelationId(Integer relationId) {
        this.relationId = relationId;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @JsonbTransient
    public Collection<FamilyDetails> getFamilyDetailsCollection() {
        return familyDetailsCollection;
    }

    public void setFamilyDetailsCollection(Collection<FamilyDetails> familyDetailsCollection) {
        this.familyDetailsCollection = familyDetailsCollection;
    }

    @JsonbTransient
    public Collection<Nominee> getNomineeCollection() {
        return nomineeCollection;
    }

    public void setNomineeCollection(Collection<Nominee> nomineeCollection) {
        this.nomineeCollection = nomineeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (relationId != null ? relationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RelationDetails)) {
            return false;
        }
        RelationDetails other = (RelationDetails) object;
        if ((this.relationId == null && other.relationId != null) || (this.relationId != null && !this.relationId.equals(other.relationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RelationDetails[ relationId=" + relationId + " ]";
    }
    
}
