/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "occupation_master")
@NamedQueries({
    @NamedQuery(name = "OccupationMaster.findAll", query = "SELECT o FROM OccupationMaster o"),
    @NamedQuery(name = "OccupationMaster.findByOccupationId", query = "SELECT o FROM OccupationMaster o WHERE o.occupationId = :occupationId"),
    @NamedQuery(name = "OccupationMaster.findByOccupation", query = "SELECT o FROM OccupationMaster o WHERE o.occupation = :occupation"),
    @NamedQuery(name = "OccupationMaster.findByOfficePhoneNumber", query = "SELECT o FROM OccupationMaster o WHERE o.officePhoneNumber = :officePhoneNumber"),
    @NamedQuery(name = "OccupationMaster.findByIsDelete", query = "SELECT o FROM OccupationMaster o WHERE o.isDelete = :isDelete"),
    @NamedQuery(name = "OccupationMaster.findByIsActive", query = "SELECT o FROM OccupationMaster o WHERE o.isActive = :isActive"),
    @NamedQuery(name = "OccupationMaster.findByAddedOn", query = "SELECT o FROM OccupationMaster o WHERE o.addedOn = :addedOn"),
    @NamedQuery(name = "OccupationMaster.findByUpdatedOn", query = "SELECT o FROM OccupationMaster o WHERE o.updatedOn = :updatedOn")})
public class OccupationMaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "OccupationId")
    private Integer occupationId;
    @Basic(optional = false)
    @Column(name = "Occupation")
    private String occupation;
    @Basic(optional = false)
    @Lob
    @Column(name = "OfficeAddress")
    private String officeAddress;
    @Basic(optional = false)
    @Column(name = "OfficePhoneNumber")
    private String officePhoneNumber;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "Username", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private PersonalDetails username;

    public OccupationMaster() {
    }

    public OccupationMaster(Integer occupationId) {
        this.occupationId = occupationId;
    }

    public OccupationMaster(Integer occupationId, String occupation, String officeAddress, String officePhoneNumber, Date addedOn) {
        this.occupationId = occupationId;
        this.occupation = occupation;
        this.officeAddress = officeAddress;
        this.officePhoneNumber = officePhoneNumber;
        this.addedOn = addedOn;
    }

    public Integer getOccupationId() {
        return occupationId;
    }

    public void setOccupationId(Integer occupationId) {
        this.occupationId = occupationId;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public String getOfficePhoneNumber() {
        return officePhoneNumber;
    }

    public void setOfficePhoneNumber(String officePhoneNumber) {
        this.officePhoneNumber = officePhoneNumber;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public PersonalDetails getUsername() {
        return username;
    }

    public void setUsername(PersonalDetails username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (occupationId != null ? occupationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OccupationMaster)) {
            return false;
        }
        OccupationMaster other = (OccupationMaster) object;
        if ((this.occupationId == null && other.occupationId != null) || (this.occupationId != null && !this.occupationId.equals(other.occupationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.OccupationMaster[ occupationId=" + occupationId + " ]";
    }
    
}
