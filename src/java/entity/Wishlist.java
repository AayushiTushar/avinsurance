/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "wishlist")
@NamedQueries({
    @NamedQuery(name = "Wishlist.findAll", query = "SELECT w FROM Wishlist w"),
    @NamedQuery(name = "Wishlist.findByWishlistId", query = "SELECT w FROM Wishlist w WHERE w.wishlistId = :wishlistId"),
    @NamedQuery(name = "Wishlist.findByIsDelete", query = "SELECT w FROM Wishlist w WHERE w.isDelete = :isDelete"),
    @NamedQuery(name = "Wishlist.findByIsActive", query = "SELECT w FROM Wishlist w WHERE w.isActive = :isActive"),
    @NamedQuery(name = "Wishlist.findByAddedOn", query = "SELECT w FROM Wishlist w WHERE w.addedOn = :addedOn"),
    @NamedQuery(name = "Wishlist.findByUpdatedOn", query = "SELECT w FROM Wishlist w WHERE w.updatedOn = :updatedOn")})
public class Wishlist implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "WishlistId")
    private Integer wishlistId;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "PolicyId", referencedColumnName = "PolicyId")
    @ManyToOne(optional = false)
    private PolicyDetails policyId;
    @JoinColumn(name = "Username", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private PersonalDetails username;

    public Wishlist() {
    }

    public Wishlist(Integer wishlistId) {
        this.wishlistId = wishlistId;
    }

    public Wishlist(Integer wishlistId, Date addedOn) {
        this.wishlistId = wishlistId;
        this.addedOn = addedOn;
    }

    public Integer getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(Integer wishlistId) {
        this.wishlistId = wishlistId;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public PolicyDetails getPolicyId() {
        return policyId;
    }

    public void setPolicyId(PolicyDetails policyId) {
        this.policyId = policyId;
    }

    public PersonalDetails getUsername() {
        return username;
    }

    public void setUsername(PersonalDetails username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wishlistId != null ? wishlistId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Wishlist)) {
            return false;
        }
        Wishlist other = (Wishlist) object;
        if ((this.wishlistId == null && other.wishlistId != null) || (this.wishlistId != null && !this.wishlistId.equals(other.wishlistId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Wishlist[ wishlistId=" + wishlistId + " ]";
    }
    
}
