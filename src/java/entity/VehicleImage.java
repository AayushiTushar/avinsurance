/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "vehicle_image")
@NamedQueries({
    @NamedQuery(name = "VehicleImage.findAll", query = "SELECT v FROM VehicleImage v"),
    @NamedQuery(name = "VehicleImage.findByVehicleImageId", query = "SELECT v FROM VehicleImage v WHERE v.vehicleImageId = :vehicleImageId"),
    @NamedQuery(name = "VehicleImage.findByImage", query = "SELECT v FROM VehicleImage v WHERE v.image = :image"),
    @NamedQuery(name = "VehicleImage.findByIsDelete", query = "SELECT v FROM VehicleImage v WHERE v.isDelete = :isDelete"),
    @NamedQuery(name = "VehicleImage.findByIsActive", query = "SELECT v FROM VehicleImage v WHERE v.isActive = :isActive"),
    @NamedQuery(name = "VehicleImage.findByAddedOn", query = "SELECT v FROM VehicleImage v WHERE v.addedOn = :addedOn"),
    @NamedQuery(name = "VehicleImage.findByUpdatedOn", query = "SELECT v FROM VehicleImage v WHERE v.updatedOn = :updatedOn")})
public class VehicleImage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "VehicleImageId")
    private Integer vehicleImageId;
    @Basic(optional = false)
    @Column(name = "Image")
    private String image;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "VehicleClaimId", referencedColumnName = "VehicleClaimId")
    @ManyToOne(optional = false)
    private VehicleClaim vehicleClaimId;

    public VehicleImage() {
    }

    public VehicleImage(Integer vehicleImageId) {
        this.vehicleImageId = vehicleImageId;
    }

    public VehicleImage(Integer vehicleImageId, String image, Date addedOn) {
        this.vehicleImageId = vehicleImageId;
        this.image = image;
        this.addedOn = addedOn;
    }

    public Integer getVehicleImageId() {
        return vehicleImageId;
    }

    public void setVehicleImageId(Integer vehicleImageId) {
        this.vehicleImageId = vehicleImageId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public VehicleClaim getVehicleClaimId() {
        return vehicleClaimId;
    }

    public void setVehicleClaimId(VehicleClaim vehicleClaimId) {
        this.vehicleClaimId = vehicleClaimId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehicleImageId != null ? vehicleImageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleImage)) {
            return false;
        }
        VehicleImage other = (VehicleImage) object;
        if ((this.vehicleImageId == null && other.vehicleImageId != null) || (this.vehicleImageId != null && !this.vehicleImageId.equals(other.vehicleImageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.VehicleImage[ vehicleImageId=" + vehicleImageId + " ]";
    }
    
}
