/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "id_proof_master")
@NamedQueries({
    @NamedQuery(name = "IdProofMaster.findAll", query = "SELECT i FROM IdProofMaster i"),
    @NamedQuery(name = "IdProofMaster.findByMasterProofId", query = "SELECT i FROM IdProofMaster i WHERE i.masterProofId = :masterProofId"),
    @NamedQuery(name = "IdProofMaster.findByName", query = "SELECT i FROM IdProofMaster i WHERE i.name = :name"),
    @NamedQuery(name = "IdProofMaster.findByIsDelete", query = "SELECT i FROM IdProofMaster i WHERE i.isDelete = :isDelete"),
    @NamedQuery(name = "IdProofMaster.findByIsActive", query = "SELECT i FROM IdProofMaster i WHERE i.isActive = :isActive"),
    @NamedQuery(name = "IdProofMaster.findByAddedOn", query = "SELECT i FROM IdProofMaster i WHERE i.addedOn = :addedOn"),
    @NamedQuery(name = "IdProofMaster.findByUpdatedOn", query = "SELECT i FROM IdProofMaster i WHERE i.updatedOn = :updatedOn")})
public class IdProofMaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MasterProofId")
    private Integer masterProofId;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "masterProofId")
    private Collection<IdProof> idProofCollection;

    public IdProofMaster() {
    }

    public IdProofMaster(Integer masterProofId) {
        this.masterProofId = masterProofId;
    }

    public IdProofMaster(Integer masterProofId, String name, Date addedOn) {
        this.masterProofId = masterProofId;
        this.name = name;
        this.addedOn = addedOn;
    }

    public Integer getMasterProofId() {
        return masterProofId;
    }

    public void setMasterProofId(Integer masterProofId) {
        this.masterProofId = masterProofId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @JsonbTransient
    public Collection<IdProof> getIdProofCollection() {
        return idProofCollection;
    }

    public void setIdProofCollection(Collection<IdProof> idProofCollection) {
        this.idProofCollection = idProofCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (masterProofId != null ? masterProofId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IdProofMaster)) {
            return false;
        }
        IdProofMaster other = (IdProofMaster) object;
        if ((this.masterProofId == null && other.masterProofId != null) || (this.masterProofId != null && !this.masterProofId.equals(other.masterProofId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.IdProofMaster[ masterProofId=" + masterProofId + " ]";
    }
    
}
