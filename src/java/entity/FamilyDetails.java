/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "family_details")
@NamedQueries({
    @NamedQuery(name = "FamilyDetails.findAll", query = "SELECT f FROM FamilyDetails f"),
    @NamedQuery(name = "FamilyDetails.findByFamilyId", query = "SELECT f FROM FamilyDetails f WHERE f.familyId = :familyId"),
    @NamedQuery(name = "FamilyDetails.findByFirstName", query = "SELECT f FROM FamilyDetails f WHERE f.firstName = :firstName"),
    @NamedQuery(name = "FamilyDetails.findByMiddleName", query = "SELECT f FROM FamilyDetails f WHERE f.middleName = :middleName"),
    @NamedQuery(name = "FamilyDetails.findByLastName", query = "SELECT f FROM FamilyDetails f WHERE f.lastName = :lastName"),
    @NamedQuery(name = "FamilyDetails.findByIsDelete", query = "SELECT f FROM FamilyDetails f WHERE f.isDelete = :isDelete"),
    @NamedQuery(name = "FamilyDetails.findByIsActive", query = "SELECT f FROM FamilyDetails f WHERE f.isActive = :isActive"),
    @NamedQuery(name = "FamilyDetails.findByAddedOn", query = "SELECT f FROM FamilyDetails f WHERE f.addedOn = :addedOn"),
    @NamedQuery(name = "FamilyDetails.findByUpdatedOn", query = "SELECT f FROM FamilyDetails f WHERE f.updatedOn = :updatedOn")})
public class FamilyDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "FamilyId")
    private Integer familyId;
    @Basic(optional = false)
    @Column(name = "FirstName")
    private String firstName;
    @Basic(optional = false)
    @Column(name = "MiddleName")
    private String middleName;
    @Basic(optional = false)
    @Column(name = "LastName")
    private String lastName;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @JoinColumn(name = "RelationId", referencedColumnName = "RelationId")
    @ManyToOne(optional = false)
    private RelationDetails relationId;
    @JoinColumn(name = "Username", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private PersonalDetails username;

    public FamilyDetails() {
    }

    public FamilyDetails(Integer familyId) {
        this.familyId = familyId;
    }

    public FamilyDetails(Integer familyId, String firstName, String middleName, String lastName, Date addedOn) {
        this.familyId = familyId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.addedOn = addedOn;
    }

    public Integer getFamilyId() {
        return familyId;
    }

    public void setFamilyId(Integer familyId) {
        this.familyId = familyId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public RelationDetails getRelationId() {
        return relationId;
    }

    public void setRelationId(RelationDetails relationId) {
        this.relationId = relationId;
    }

    public PersonalDetails getUsername() {
        return username;
    }

    public void setUsername(PersonalDetails username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (familyId != null ? familyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FamilyDetails)) {
            return false;
        }
        FamilyDetails other = (FamilyDetails) object;
        if ((this.familyId == null && other.familyId != null) || (this.familyId != null && !this.familyId.equals(other.familyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.FamilyDetails[ familyId=" + familyId + " ]";
    }
    
}
