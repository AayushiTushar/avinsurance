/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "policy_details")
@NamedQueries({
    @NamedQuery(name = "PolicyDetails.findAll", query = "SELECT p FROM PolicyDetails p"),
    @NamedQuery(name = "PolicyDetails.findByPolicyId", query = "SELECT p FROM PolicyDetails p WHERE p.policyId = :policyId"),
    @NamedQuery(name = "PolicyDetails.findByPolicyName", query = "SELECT p FROM PolicyDetails p WHERE p.policyName = :policyName"),
    @NamedQuery(name = "PolicyDetails.findByPremiumyearly", query = "SELECT p FROM PolicyDetails p WHERE p.premiumyearly = :premiumyearly"),
    @NamedQuery(name = "PolicyDetails.findByMaxterm", query = "SELECT p FROM PolicyDetails p WHERE p.maxterm = :maxterm"),
    @NamedQuery(name = "PolicyDetails.findByIsDelete", query = "SELECT p FROM PolicyDetails p WHERE p.isDelete = :isDelete"),
    @NamedQuery(name = "PolicyDetails.findByIsActive", query = "SELECT p FROM PolicyDetails p WHERE p.isActive = :isActive"),
    @NamedQuery(name = "PolicyDetails.findByAddedOn", query = "SELECT p FROM PolicyDetails p WHERE p.addedOn = :addedOn"),
    @NamedQuery(name = "PolicyDetails.findByUpdatedOn", query = "SELECT p FROM PolicyDetails p WHERE p.updatedOn = :updatedOn")})
public class PolicyDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PolicyId")
    private Integer policyId;
    @Basic(optional = false)
    @Column(name = "PolicyName")
    private String policyName;
    @Basic(optional = false)
    @Column(name = "Premium_yearly")
    private int premiumyearly;
    @Basic(optional = false)
    @Column(name = "Max_term")
    private int maxterm;
    @Basic(optional = false)
    @Lob
    @Column(name = "Description")
    private String description;
    @Column(name = "isDelete")
    private Boolean isDelete;
    @Column(name = "isActive")
    private Boolean isActive;
    @Basic(optional = false)
    @Column(name = "AddedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "UpdatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "policyId")
    private Collection<UserPolicyDetails> userPolicyDetailsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "policyId")
    private Collection<Wishlist> wishlistCollection;
    @JoinColumn(name = "InsuranceId", referencedColumnName = "InsuranceId")
    @ManyToOne(optional = false)
    private InsuranceType insuranceId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "policyId")
    private Collection<PremiumDetails> premiumDetailsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "policyId")
    private Collection<VehicleDetails> vehicleDetailsCollection;

    public PolicyDetails() {
    }

    public PolicyDetails(Integer policyId) {
        this.policyId = policyId;
    }

    public PolicyDetails(Integer policyId, String policyName, int premiumyearly, int maxterm, String description, Date addedOn) {
        this.policyId = policyId;
        this.policyName = policyName;
        this.premiumyearly = premiumyearly;
        this.maxterm = maxterm;
        this.description = description;
        this.addedOn = addedOn;
    }

    public Integer getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Integer policyId) {
        this.policyId = policyId;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public int getPremiumyearly() {
        return premiumyearly;
    }

    public void setPremiumyearly(int premiumyearly) {
        this.premiumyearly = premiumyearly;
    }

    public int getMaxterm() {
        return maxterm;
    }

    public void setMaxterm(int maxterm) {
        this.maxterm = maxterm;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @JsonbTransient
    public Collection<UserPolicyDetails> getUserPolicyDetailsCollection() {
        return userPolicyDetailsCollection;
    }

    public void setUserPolicyDetailsCollection(Collection<UserPolicyDetails> userPolicyDetailsCollection) {
        this.userPolicyDetailsCollection = userPolicyDetailsCollection;
    }

    @XmlTransient
    public Collection<Wishlist> getWishlistCollection() {
        return wishlistCollection;
    }

    public void setWishlistCollection(Collection<Wishlist> wishlistCollection) {
        this.wishlistCollection = wishlistCollection;
    }

    public InsuranceType getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(InsuranceType insuranceId) {
        this.insuranceId = insuranceId;
    }

    @JsonbTransient
    public Collection<PremiumDetails> getPremiumDetailsCollection() {
        return premiumDetailsCollection;
    }

    public void setPremiumDetailsCollection(Collection<PremiumDetails> premiumDetailsCollection) {
        this.premiumDetailsCollection = premiumDetailsCollection;
    }

   @JsonbTransient
    public Collection<VehicleDetails> getVehicleDetailsCollection() {
        return vehicleDetailsCollection;
    }

    public void setVehicleDetailsCollection(Collection<VehicleDetails> vehicleDetailsCollection) {
        this.vehicleDetailsCollection = vehicleDetailsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (policyId != null ? policyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PolicyDetails)) {
            return false;
        }
        PolicyDetails other = (PolicyDetails) object;
        if ((this.policyId == null && other.policyId != null) || (this.policyId != null && !this.policyId.equals(other.policyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PolicyDetails[ policyId=" + policyId + " ]";
    }
    
}
