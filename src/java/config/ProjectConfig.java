/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;
import org.glassfish.soteria.identitystores.hash.Pbkdf2PasswordHashImpl;

/**
 *
 * @author 
 */

@DatabaseIdentityStoreDefinition(
        dataSourceLookup = "jdbc/insurace",
        callerQuery = "SELECT Password FROM personal_details WHERE Username = ?",
        groupsQuery = "SELECT r.RoleName FROM user_role r, personal_details u WHERE r.RoleId = u.RoleId AND u.Username = ?",
        hashAlgorithm = Pbkdf2PasswordHashImpl.class,
        priority = 30
    )

@Named(value = "projectConfig")
@ApplicationScoped
public class ProjectConfig {
    public ProjectConfig() {
        System.err.println("Project Config Initialized!!");
    }
}
