/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest;

import SeessionBean.InsuranceLocal;
import entity.City;
import entity.Contactus;
import entity.EducationDetails;
import entity.FamilyDetails;
import entity.HealthInfo;
import entity.IdProof;
import entity.IdProofMaster;
import entity.InsuranceType;
import entity.LifeClaim;
import entity.Nominee;
import entity.OccupationMaster;
import entity.PersonalDetails;
import entity.PolicyDetails;
import entity.RelationDetails;
import entity.State;
import entity.UserPolicyDetails;
import entity.VehicleClaim;
import entity.VehicleDetails;
import entity.VehicleImage;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Admin
 */
@Path("Insurance")
@DeclareRoles({"Admin","User"})//@RequestScoped
public class InsuranceResource {

    @EJB InsuranceLocal bl;
     
    @Context
    private UriInfo context;
    
    
    public InsuranceResource() {
    }
  
    @POST    
    @Path("register/{fname}/{lname}/{uname}/{email}/{pass}/{roleid}")
    public void registration(@PathParam("fname") String firstname,@PathParam("lname") String lastname,@PathParam("uname") String username,@PathParam("email") String email,@PathParam("pass") String password,@PathParam("roleid") int roleid){
        
        bl.registration(firstname, lastname, username, email, password, roleid);
    }

    
    // Insurance
    
    @RolesAllowed("Admin")
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllInsurance")
    public Collection<InsuranceType> showAllInsurance()
    {
        return bl.showAllInsurance();
    }
    
    
    @RolesAllowed("Admin")
    @GET
    @Path("getInsurance/{id}")
    @Produces (MediaType.APPLICATION_JSON)
    public InsuranceType getInsurance(@PathParam("id") int insuranceid)
    {
        return bl.getInsurance(insuranceid);
    }
    
    
    @RolesAllowed("Admin")
    @POST
    @Path("addInsurance/{name}/{description}")
    public void addInsurance(@PathParam("name") String name,@PathParam("description") String description)
    {
        bl.addInsurance(name, description);
    }
    
    
    @RolesAllowed("Admin")
    @POST 
    @Path("updateInsurance/{id}/{name}/{description}")
    public void updateInsurance(@PathParam("id") int insuranceid,@PathParam("name") String name,@PathParam("description") String description)
    {
        bl.updateInsurance(insuranceid, name, description);
    }
    
    
    @RolesAllowed("Admin")
    @DELETE   
    @Path("deleteInsurance/{id}")
    public void deleteInsurance(@PathParam("id") int insuranceid)
    {
        bl.deleteInsurance(insuranceid);      
    }
    
    
    // City
    
    @RolesAllowed("Admin")
    @POST
    @Path("addCity/{name}/{stateid}")
    public void addCity(@PathParam("name") String cityname,@PathParam("stateid") int stateid)
    {
        bl.addCity(cityname, stateid);
    }
    
    @RolesAllowed("Admin")
    @POST
    @Path("updateCity/{id}/{name}/{stateid}")
    public void updateCity(@PathParam("id") int cityid,@PathParam("name") String cityname,@PathParam("stateid") int stateid)
    {
        bl.updateCity(cityid, cityname, stateid);
    }
    
    @RolesAllowed("Admin")
    @DELETE
    @Path("deleteCity/{id}")
    public void deleteCity(@PathParam("id") int cityid)
    {
        bl.deleteCity(cityid);
    }
    
    
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllCity")
    public Collection<City> showAllCity()
    {
        return bl.showAllCity();
    }
    
    @RolesAllowed("Admin")
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("getCitys/{id}")
    public City getCity(@PathParam("id") int cityid)
    {
        return bl.getCity(cityid);
    }
    
    
    // State
    
    @RolesAllowed("Admin")
    @POST
    @Path("addStates/{name}")
    public void addState(@PathParam("name") String statename)
    {
        bl.addState(statename);
    }
    
    @RolesAllowed("Admin")
    @POST
    @Path("updateState/{id}/{name}")
    public void updateState(@PathParam("id") int stateid,@PathParam("name") String statename)
    {
        bl.updateState(stateid, statename);
    }
    
    @RolesAllowed("Admin")
    @DELETE
    @Path("deleteState/{id}")
    public void deleteState(@PathParam("id") int stateid)
    {
        bl.deleteState(stateid);
    }
    
    
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllState")
    public Collection<State> showAllState()
    {
        return bl.showAllState();
    }
    
    @RolesAllowed("Admin")
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("getState/{id}")
    public State getState(@PathParam("id") int stateid)
    {
        return bl.getState(stateid);
    }
    
    
    // Policy 
    
    @RolesAllowed("Admin")
    @POST
    @Path("addPolicy/{name}/{year}/{term}/{desc}/{insid}")
    public void addPolicy(@PathParam("name") String policyname,@PathParam("year") int primium_year,@PathParam("term") int max_term,@PathParam("desc") String description,@PathParam("insid") int insuranceid)
    {
        bl.addPolicy(policyname, primium_year, max_term, description, insuranceid);
    }
    
    @RolesAllowed("Admin")
    @POST
    @Path("updatePolicy/{pid}/{name}/{year}/{term}/{desc}/{insid}")
    public void updatePolicy(@PathParam("pid") int policyid,@PathParam("name") String policyname,@PathParam("year") int primium_year,@PathParam("term") int max_term,@PathParam("desc") String description,@PathParam("insid") int insuranceid)
    {
        bl.updatePolicy(policyid, policyname, primium_year, max_term, description, insuranceid);
    }
    
    @RolesAllowed("Admin")
    @DELETE
    @Path("deletePolicy/{pid}")
    public void deletePolicy(int policyid)
    {
        bl.deletePolicy(policyid);
    }
    
    @RolesAllowed("Admin")
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllPolicy")
    public Collection<PolicyDetails> showAllPolicy()
    {
        return bl.showAllPolicy();
    }
    
    @RolesAllowed("Admin")
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("getPolicy/{id}")
    public PolicyDetails getPolicy(@PathParam("id") int policyid)
    {
        return bl.getPolicy(policyid);
    }
    
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllInsurancePolicy/{id}")
    public Collection<PolicyDetails> showAllInsurancePolicy(@PathParam("id") int insuranceid)
    {
        return bl.showAllInsurancePolicy(insuranceid);
    }
    
    
    //Relation Master    
    
    @RolesAllowed("Admin")
    @POST
    @Path("addRelation/{name}")
    public void addRelation(@PathParam("name") String relation)
    {
        bl.addRelation(relation);
    }
    
    @RolesAllowed("Admin")
    @POST
    @Path("updateRelation/{id}/{name}")
    public void updateRelation(@PathParam("id") int relationid,@PathParam("name") String relation)
    {
        bl.updateRelation(relationid, relation);
    }
    
    @RolesAllowed("Admin")
    @DELETE
    @Path("deleteRelation/{id}")
    public void deleteRelation(int relationid)
    {
        bl.deleteRelation(relationid);
    }
    
    
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllRelations")
    public Collection<RelationDetails> showAllRelations()
    {
        return bl.showAllRelations();
    }
    
    @RolesAllowed("Admin")
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("getRelation/{id}")
    public RelationDetails getRelation(@PathParam("id") int relationid)
    {
        return bl.getRelation(relationid);
    }
    
    
    //ID Proff Master   
    
    @RolesAllowed("Admin")
    @POST
    @Path("addIdProofMaster/{name}")
    public void addIdProofMaster(@PathParam("name") String proofname)
    {
        bl.addIdProofMaster(proofname);
    }
    
    @RolesAllowed("Admin")
    @POST
    @Path("addIdProofMaster/{id}/{name}")
    public void updateIdProofMaster(@PathParam("id") int masterproffid,@PathParam("name") String proofname)
    {
        bl.updateIdProofMaster(masterproffid, proofname);
    }
    
    @RolesAllowed("Admin")
    @DELETE
    @Path("deleteIdProofMaster/{id}")
    public void deleteIdProofMaster(int masterproofid)
    {
        bl.deleteIdProofMaster(masterproofid);
    }
    
    
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllIdProofTypes")
    public Collection<IdProofMaster> showAllIdProofTypes()
    {
        return bl.showAllIdProofTypes();
    }
    
    @RolesAllowed("Admin")
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("getProofMaster/{id}")
    public IdProofMaster getProofMaster(@PathParam("id") int masterproofid)
    {
        return bl.getProofMaster(masterproofid);
    }
    
    
    // Life claim
            
    @RolesAllowed("User")
    @POST
    @Path("addLifeClaim/{upid}/{deathimg}/{dodeath}/{status}/{desc}")
    public void addLifeClaim(@PathParam("upid") int userpolicyid,@PathParam("deathimg") String deathcertificate,@PathParam("dodeath") String dateofdeath,@PathParam("status") String status,@PathParam("desc") String description) 
    {
        bl.addLifeClaim(userpolicyid, deathcertificate, dateofdeath,status, description);
    }
    
    @RolesAllowed("Admin")
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllLifeClaims")
    public Collection<LifeClaim> showAllLifeClaims()
    {
        return bl.showAllLifeClaims();
    }
       
    
    // Vehicle claim
    
    @RolesAllowed("User")
    @POST
    @Path("addVehicleClaim/{upid}/{desc}/{status}/{bill}")
    public void addVehicleClaim(@PathParam("upid") int userpolicyid,@PathParam("desc") String description,@PathParam("status") String status,@PathParam("bill") String vehicalbill)
    {
        bl.addVehicleClaim(userpolicyid, description, status, vehicalbill);
    } 
    
    @RolesAllowed("Admin")
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllVehicleClaim")
    public Collection<VehicleClaim> showAllVehicleClaim()
    {
        return bl.showAllVehicleClaim();
    }
    
    
    // Vehicle Image
    
    @RolesAllowed("Admin")
    @POST
    @Path("addVehicleImage/{imgs}/{vcid}")
    public void addVehicleImage(@PathParam("imgs") String images,@PathParam("vcid") int vclaimid)
    {
        bl.addVehicleImage(images, vclaimid);
    }
    
    @RolesAllowed("Admin")
    @GET   
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllVehicleImage")
    public Collection<VehicleImage> showAllVehicleImage()
    {
        return bl.showAllVehicleImage();
    }

    // Education 
        
    @RolesAllowed("User")
    @POST
    @Path("addEducation/{lc}/{marksheet}/{username}")
    public void addEducation(@PathParam("lc") String lc,@PathParam("marksheet") String marksheet,@PathParam("username") String username)
    {
        bl.addEducation(lc, marksheet, username);
    }
    
    
    @RolesAllowed("Admin")
    @GET    
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showEducation")
    public Collection<EducationDetails> showEducation()
    {
        return bl.showEducation();
    }
    
    
    // Personal
    
    @RolesAllowed("User")
    @POST 
    @Path("updateInfo/{username}/{mname}/{add}/{gen}/{dob}/{age}/{phone}/{pincode}/{cityid}/{stateid}/{photo}")
    public void updateInfo(@PathParam("username") String username,@PathParam("mname") String middlename,@PathParam("add") String address,@PathParam("gen") String gender,@PathParam("dob") String dob,@PathParam("age") int age,@PathParam("phone") String phone,@PathParam("pincode") int pincode,@PathParam("cityid") int cityid,@PathParam("stateid") int stateid,@PathParam("photo") String photo)
    {
        System.err.println("In Resourece .........................");
        System.err.println("Resource Date = "+ dob);
       
        bl.updatePersonalDetails(username, middlename, address, gender, dob, age, phone, pincode, cityid, stateid, photo);
        
    }
    
    
    @RolesAllowed("Admin")
    @GET    
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllPersonalInfo")
    public Collection<PersonalDetails> showAllPersonalInfo()
    {
        return bl.showAllPersonalInfo();
    }
     
    
    // Family
    
    @RolesAllowed("User")
    @POST
    @Path("addFamily/{fname}/{mname}/{lname}/{username}/{relationid}")
    public void addFamily(@PathParam("fname") String firstname,@PathParam("mname") String middlename,@PathParam("lname") String lastname,@PathParam("username") String username,@PathParam("relationid") int relationid)
    {
        bl.addFamily(firstname, middlename, lastname, username, relationid);
    }
    
    
    @RolesAllowed("Admin")
    @GET    
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllFamilyDetails")
    public Collection<FamilyDetails> showAllFamilyDetails()
    {
        return bl.showAllFamilyDetails();
    }

    
    // Occupation
    
    @RolesAllowed("User")
    @POST 
    @Path("addOccupations/{occupations}/{add}/{phone}/{username}")
    public void addOccupation(@PathParam("occupations") String occupation,@PathParam("add") String officeadd,@PathParam("phone") String officenumber,@PathParam("username") String username)
    {
        bl.addOccupation(occupation, officeadd, officenumber, username);
    }
    
    
    @RolesAllowed("Admin")
    @GET    
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllOccupation")
    public Collection<OccupationMaster> showAllOccupation()
    {
        return bl.showAllOccupation();
    }

    
    // IdProof
      
    @RolesAllowed("User")
    @POST 
    @Path("addIdProff/{masterproffid}/{number}/{image}/{username}")
    public void addIdProff(@PathParam("masterproffid") int masterproffid, @PathParam("number") String number,@PathParam("image") String image,@PathParam("username") String username)
    {
        bl.addIdProff(masterproffid, number, image, username);
    }
    
    
    @RolesAllowed("Admin")
    @GET    
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllIdProffs")
    public Collection<IdProof> showAllIdProffs()
    {
        return bl.showAllIdProffs();
    }

    
    // Nominee
    
    @RolesAllowed("User")
    @POST   
    @Path("addNominee/{fname}/{mname}/{lname}/{occupation}/{add}/{phone}/{lc}/{marksheet}/{username}/{relationid}")
    public void addNominee(@PathParam("fname") String firstname,@PathParam("mname") String middlename,@PathParam("lname") String lastname,@PathParam("occupation") String occupation,@PathParam("add") String officeadd,@PathParam("phone") String officenumber,@PathParam("lc") String lc,@PathParam("marksheet") String marksheet,@PathParam("username") String username,@PathParam("relationid") int relationid)
    {
        bl.addNominee(firstname, middlename, lastname, occupation, officeadd, officenumber, lc, marksheet, username, relationid);
    }
    
    
    @RolesAllowed("Admin")
    @GET    
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllNominee")
    public Collection<Nominee> showAllNominee()
    {
        return bl.showAllNominee();
    }
    
    
    // Vehicle details
        
    @RolesAllowed("User")
    @POST   
    @Path("addVehicle/{policyid}/{modelno}/{rcbookno}/{numberplate}/{chesiesno}/{bodystyle}/{rcbookimg}/{username}/{boughtyear}/{licenceimg}")
    public void addVehicle(@PathParam("policyid") int policyid,@PathParam("modelno") String modelno,@PathParam("rcbookno") String rcbookno,@PathParam("numberplate") String numberplate,@PathParam("chesiesno") String chesiesno,@PathParam("bodystyle") String bodystyle,@PathParam("rcbookimg") String rcbookimg,@PathParam("username") String username,@PathParam("boughtyear") String boughtyear,@PathParam("licenceimg") String licenceimg)
    {
        bl.addVehicle(policyid, modelno, rcbookno, numberplate, chesiesno, bodystyle, rcbookimg, username, boughtyear, licenceimg);
    }
    
    
    @RolesAllowed("Admin")
    @GET    
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllVehicle")
    public Collection<VehicleDetails> showAllVehicle()
    {
        return bl.showAllVehicle();
    }
    
    
    // Health details
    
    @RolesAllowed("User")
    @POST   
    @Path("addHealthInfo/{disease}/{desc}/{document}/{username}")
    public void addHealthInfo(@PathParam("disease") String disease,@PathParam("desc") String description,@PathParam("document") String document,@PathParam("username") String username)
    {
        bl.addHealthInfo(disease, description, document, username);
    }
    
    
    @RolesAllowed("Admin")
    @GET    
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllHealthInfo")
    public Collection<HealthInfo> showAllHealthInfo()
    {
        return bl.showAllHealthInfo();
    }
    
    
    // Contactcus
        
    
    @POST   
    @Path("addContactus/{name}/{email}/{sub}/{mess}")
    public void addContactus(@PathParam("name") String name,@PathParam("email") String email,@PathParam("sub") String subject,@PathParam("mess") String message)
    {
        bl.addContactus(name, email, subject, message);
    }
    
    
    @RolesAllowed("Admin")
    @GET    
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllContactDetails")
    public Collection<Contactus> showAllContactDetails()
    {
        return bl.showAllContactDetails();
    }

    
    // UserPolicyDetails
    
    @GET
    @Produces (MediaType.APPLICATION_JSON)
    @Path("showAllUserPolicy")
    public Collection<UserPolicyDetails> showAllUserPolicy()
    {
        return bl.showAllUserPolicy();
    }
   
    
}
