/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import java.io.IOException;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;
/**
 *
 * @author Admin
 */
@Provider
@PreMatching
public class RestFilter implements ClientRequestFilter{
    public static String mytoken;
    
    public RestFilter(String token)
    {
        mytoken = token;
    }

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        System.err.println("In Form Auth Client Filter" + mytoken);
        
        Cookie c = new Cookie("JREMEMBERMEID", mytoken);
        requestContext.getHeaders().add(HttpHeaders.AUTHORIZATION, "Bearer "+mytoken);
        requestContext.getHeaders().add(HttpHeaders.COOKIE, c);
        
        System.err.println("After Cookie Header auth client filter:" + mytoken);
    }
    
}

