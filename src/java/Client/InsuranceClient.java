/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

/**
 * Jersey REST client generated for REST resource:InsuranceResource
 * [Insurance]<br>
 * USAGE:
 * <pre>
 *        InsuranceClient client = new InsuranceClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author Admin
 */
public class InsuranceClient {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/AVInsurance_Project/webresources";

    public InsuranceClient() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("Insurance");
    }

    public void registration(String fname, String lname, String uname, String email, String pass, String roleid) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("register/{0}/{1}/{2}/{3}/{4}/{5}", new Object[]{fname, lname, uname, email, pass, roleid})).request().post(null);
    }

    public void addContactus(String name, String email, String sub, String mess) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addContactus/{0}/{1}/{2}/{3}", new Object[]{name, email, sub, mess})).request().post(null);
    }
    
    public void close() {
        client.close();
    }
    
}
