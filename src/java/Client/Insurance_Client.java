/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

/**
 * Jersey REST client generated for REST resource:InsuranceResource
 * [Insurance]<br>
 * USAGE:
 * <pre>
 *        Insurance_Client client = new Insurance_Client();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author Admin
 */
public class Insurance_Client {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/AVInsurance_Project/webresources";

    public Insurance_Client(String token) {
       
        client = javax.ws.rs.client.ClientBuilder.newClient();
        client.register(new RestFilter(token));
        webTarget = client.target(BASE_URI).path("Insurance");
                   
      }
static {
        //for localhost testing only
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                new javax.net.ssl.HostnameVerifier() {

            public boolean verify(String hostname,
                    javax.net.ssl.SSLSession sslSession) {
                if (hostname.equals("localhost")) {
                    return true;
                }
                return false;
            }
        });
        
    }

    public void updateCity(String id, String name, String stateid) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("updateCity/{0}/{1}/{2}", new Object[]{id, name, stateid})).request().post(null);
    }

    public void addVehicleClaim(String upid, String desc, String status, String bill) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addVehicleClaim/{0}/{1}/{2}/{3}", new Object[]{upid, desc, status, bill})).request().post(null);
    }

    public <T> T getProofMaster(Class<T> responseType, String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("getProofMaster/{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void addLifeClaim(String upid, String deathimg, String dodeath, String status, String desc) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addLifeClaim/{0}/{1}/{2}/{3}/{4}", new Object[]{upid, deathimg, dodeath, status, desc})).request().post(null);
    }

    public <T> T showAllLifeClaims(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllLifeClaims");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public <T> T showAllIdProofTypes(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllIdProofTypes");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void addIdProofMaster(String name) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addIdProofMaster/{0}", new Object[]{name})).request().post(null);
    }

    public <T> T showAllPolicy(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllPolicy");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public <T> T showAllContactDetails(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllContactDetails");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void deleteCity(String id) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("deleteCity/{0}", new Object[]{id})).request().delete();
    }

    public void deleteInsurance(String id) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("deleteInsurance/{0}", new Object[]{id})).request().delete();
    }

    public void addPolicy(String name, String year, String term, String desc, String insid) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addPolicy/{0}/{1}/{2}/{3}/{4}", new Object[]{name, year, term, desc, insid})).request().post(null);
    }

    public void addInsurance(String name, String description) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addInsurance/{0}/{1}", new Object[]{name, description})).request().post(null);
    }

    public void addIdProff(String masterproffid, String number, String image, String username) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addIdProff/{0}/{1}/{2}/{3}", new Object[]{masterproffid, number, image, username})).request().post(null);
    }

    public <T> T getCity(Class<T> responseType, String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("getCitys/{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void updateInsurance(String id, String name, String description) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("updateInsurance/{0}/{1}/{2}", new Object[]{id, name, description})).request().post(null);
    }

    public <T> T showAllInsurancePolicy(Class<T> responseType, String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("showAllInsurancePolicy/{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public <T> T getPolicy(Class<T> responseType, String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("getPolicy/{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void addCity(String name, String stateid) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addCity/{0}/{1}", new Object[]{name, stateid})).request().post(null);
    }

    public <T> T showAllUserPolicy(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllUserPolicy");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public <T> T showAllState(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllState");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void updateRelation(String id, String name) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("updateRelation/{0}/{1}", new Object[]{id, name})).request().post(null);
    }

    public void addFamily(String fname, String mname, String lname, String username, String relationid) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addFamily/{0}/{1}/{2}/{3}/{4}", new Object[]{fname, mname, lname, username, relationid})).request().post(null);
    }

    public void addOccupation(String occupations, String add, String phone, String username) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addOccupations/{0}/{1}/{2}/{3}", new Object[]{occupations, add, phone, username})).request().post(null);
    }

    public void addNominee(String fname, String mname, String lname, String occupation, String add, String phone, String lc, String marksheet, String username, String relationid) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addNominee/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}/{8}/{9}", new Object[]{fname, mname, lname, occupation, add, phone, lc, marksheet, username, relationid})).request().post(null);
    }

    public <T> T showAllRelations(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllRelations");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void registration(String fname, String lname, String uname, String email, String pass, String roleid) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("register/{0}/{1}/{2}/{3}/{4}/{5}", new Object[]{fname, lname, uname, email, pass, roleid})).request().post(null);
    }

    public <T> T showAllNominee(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllNominee");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public <T> T getRelation(Class<T> responseType, String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("getRelation/{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public <T> T showAllPersonalInfo(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllPersonalInfo");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void updateInfo(String username, String mname, String add, String gen, String dob, String age, String phone, String pincode, String cityid, String stateid, String photo) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("updateInfo/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}/{8}/{9}/{10}", new Object[]{username, mname, add, gen, dob, age, phone, pincode, cityid, stateid, photo})).request().post(null);
    }

    public <T> T showAllHealthInfo(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllHealthInfo");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void updateIdProofMaster(String id, String name) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addIdProofMaster/{0}/{1}", new Object[]{id, name})).request().post(null);
    }

    public <T> T showAllInsurance(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllInsurance");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void deleteRelation(String id) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("deleteRelation/{0}", new Object[]{id})).request().delete();
    }

    public <T> T showAllCity(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllCity");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void updatePolicy(String pid, String name, String year, String term, String desc, String insid) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("updatePolicy/{0}/{1}/{2}/{3}/{4}/{5}", new Object[]{pid, name, year, term, desc, insid})).request().post(null);
    }

    public <T> T showAllOccupation(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllOccupation");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void addVehicleImage(String imgs, String vcid) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addVehicleImage/{0}/{1}", new Object[]{imgs, vcid})).request().post(null);
    }

    public <T> T showEducation(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showEducation");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }    

    public void addRelation(String name) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addRelation/{0}", new Object[]{name})).request().post(null);
    }

    public void addState(String name) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addStates/{0}", new Object[]{name})).request().post(null);
    }

    public void deleteState(String id) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("deleteState/{0}", new Object[]{id})).request().delete();
    }

    public void updateState(String id, String name) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("updateState/{0}/{1}", new Object[]{id, name})).request().post(null);
    }

    public void addHealthInfo(String disease, String desc, String document, String username) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addHealthInfo/{0}/{1}/{2}/{3}", new Object[]{disease, desc, document, username})).request().post(null);
    }

    public <T> T showAllVehicleClaim(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllVehicleClaim");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void deleteIdProofMaster(String id) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("deleteIdProofMaster/{0}", new Object[]{id})).request().delete();
    }

    public void addVehicle(String policyid, String modelno, String rcbookno, String numberplate, String chesiesno, String bodystyle, String rcbookimg, String username, String boughtyear, String licenceimg) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addVehicle/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}/{8}/{9}", new Object[]{policyid, modelno, rcbookno, numberplate, chesiesno, bodystyle, rcbookimg, username, boughtyear, licenceimg})).request().post(null);
    }

    public <T> T getInsurance(Class<T> responseType, String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("getInsurance/{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public <T> T getState(Class<T> responseType, String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("getState/{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public <T> T showAllVehicleImage(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllVehicleImage");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void addEducation(String lc, String marksheet, String username) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("addEducation/{0}/{1}/{2}", new Object[]{lc, marksheet, username})).request().post(null);
    }

    public <T> T showAllFamilyDetails(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllFamilyDetails");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void deletePolicy(String pid) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("deletePolicy/{0}", new Object[]{pid})).request().delete();
    }

    public <T> T showAllIdProffs(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllIdProffs");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public <T> T showAllVehicle(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("showAllVehicle");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void close() {
        client.close();
    }
    
}
